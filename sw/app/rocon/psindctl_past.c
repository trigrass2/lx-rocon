/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  psindctl_past.c - ROCON power control and status

  (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <system_def.h>
#include <spi_drv.h>
#include <inttypes.h>
#include <string.h>
#include <pxmc.h>
#include <lt_timer.h>

#include "appl_defs.h"
#include "appl_pxmc.h"
#include "psindctl_spi.h"
#include "psindctl_past.h"

unsigned int psindctl_past_chan2ind[] = {
 0,
 2,
 4,
 6,
 1,
 3,
 5,
 7,
};

int
psindctl_past_init(void)
{
  int res;
  uint32_t mask, state;

  res = psindctl_spi_init();
  if (res < 0)
    return res;

  mask = PSINDCTL_LED_ERR_R_m | PSINDCTL_LED_ERR_G_m;
  state = PSINDCTL_LED_ERR_R_m | PSINDCTL_LED_ERR_G_m;
  psindctl_out_data_update(mask, state);

  return 0;
}

int
psindctl_past_poll(void)
{
  unsigned long flags;
  int chan;
  pxmc_state_t *mcs;
  uint32_t mask = 0;
  uint32_t state = 0;
  int flg = 0;
  uint32_t keysandst;
  uint32_t keysandst_prew;
  uint32_t keys;
  uint32_t keys_prew;
  uint32_t keys_mask;
  static uint32_t keys_confirmed;
  static uint32_t keys_changing;
  static lt_mstime_t ltime_push;
  lt_mstime_t ltime;

  lt_mstime_update();
  ltime = actual_msec;

  save_and_cli(flags);
  keysandst = psindctl_spi_state.data_in;
  keysandst_prew = psindctl_spi_state.data_in_old;
  restore_flags(flags);

  __memory_barrier();

  pxmc_for_each_mcs(chan, mcs) {
    if(mcs) {
      uint32_t axis_g = 0;
      uint32_t axis_r = 0;
      int indidx;
      if (chan < sizeof(psindctl_past_chan2ind)/
          sizeof(*psindctl_past_chan2ind)) {
        indidx = psindctl_past_chan2ind[chan];
        axis_g = PSINDCTL_LED_AXIS_X_G_m(indidx);
        axis_r = PSINDCTL_LED_AXIS_X_R_m(indidx);
      }
      mask |= axis_g | axis_r;
      flg |= mcs->pxms_flg;
      if (mcs->pxms_flg & PXMS_BSY_m)
        state |= axis_g;
      if (mcs->pxms_flg & PXMS_ERR_m)
        state |= axis_r;
    }
  }

  mask |= PSINDCTL_LED_BUSY_G_m;
  mask |= PSINDCTL_LED_ERR_R_m | PSINDCTL_LED_ERR_G_m;
  if (flg & PXMS_BSY_m)
    state |= PSINDCTL_LED_BUSY_G_m;
  if (flg & PXMS_ERR_m)
    state |= PSINDCTL_LED_ERR_R_m;

  mask |= PSINDCTL_LED_ARM_SW_m | PSINDCTL_LED_STOP_SW_m;
  if (!(keysandst & keysandst_prew & PSINDCTL_SW_RELAY_m)) {
    state |= PSINDCTL_LED_ARM_SW_m;
  } else {
    if ((ltime & 0x200) || !(flg & PXMS_ERR_m))
      state |= PSINDCTL_LED_STOP_SW_m;
  }

  if ((keysandst & keysandst_prew & PSINDCTL_SW_FORCED1_m) &&
      (~keysandst & ~keysandst_prew & PSINDCTL_SW_FORCED0_m)) {
    keys_mask = PSINDCTL_SW_ARM_m | PSINDCTL_SW_STOP_m |
                PSINDCTL_SW_SAFETY_m;
    keys = ~keysandst & keys_mask;
    keys_prew = ~keysandst_prew & keys_mask;

    if (((keys_confirmed ^ keys) != keys_changing) ||
        ((keys_confirmed ^ keys_prew) != keys_changing))
    {
      ltime_push = ltime;
      keys_changing = keys_confirmed ^ keys;
    } else {
      if ((keys & keys_prew & ~keys_confirmed & PSINDCTL_SW_STOP_m) &&
          (flg & (PXMS_ENR_m | PXMS_ENO_m))) {
        keys_confirmed |= PSINDCTL_SW_STOP_m;
        pxmc_for_each_mcs(chan, mcs) {
          if (mcs->pxms_flg & (PXMS_ENR_m | PXMS_ENO_m)) {
            pxmc_axis_release(mcs);
          }
        }
        mask |= PSINDCTL_PWR_ON_ENABLE_m;
        state |= PSINDCTL_PWR_ON_ENABLE_m * 0;
      } else if (keys_changing &&
          ((lt_msdiff_t)(ltime-ltime_push) >= 200)) {
        keys_confirmed ^= keys_changing;
        if (keys_confirmed & keys_changing & PSINDCTL_SW_STOP_m) {
          if (flg & PXMS_ERR_m) {
            pxmc_for_each_mcs(chan, mcs) {
              if (mcs->pxms_flg & (PXMS_ERR_m | PXMS_ENR_m | PXMS_ENO_m)) {
                pxmc_axis_release(mcs);
                pxmc_clear_flag(mcs, PXMS_ERR_b);
              }
            }
          }
          mask |= PSINDCTL_PWR_ON_ENABLE_m;
          state |= PSINDCTL_PWR_ON_ENABLE_m * 0;
        } else if (keys_confirmed & keys_changing & PSINDCTL_SW_ARM_m) {
          mask |= PSINDCTL_PWR_ON_ENABLE_m;
          if (pxmc_regpwron_get())
            state |= PSINDCTL_PWR_ON_ENABLE_m;
        }
      }
    }
  } else {
    if ((~keysandst & ~keysandst_prew & PSINDCTL_SW_FORCED1_m) ||
        (keysandst & keysandst_prew & PSINDCTL_SW_FORCED0_m)) {
      mask |= PSINDCTL_PWR_ON_ENABLE_m;
      state |= PSINDCTL_PWR_ON_ENABLE_m * 0;
      pxmc_for_each_mcs(chan, mcs) {
        if (mcs->pxms_flg & (PXMS_ENR_m | PXMS_ENO_m)) {
          pxmc_axis_release(mcs);
        }
      }
    }
  }

  /*
  mask |= PSINDCTL_PWR_ON_PULSE_m;
  state |= PSINDCTL_PWR_ON_PULSE_m;
  */

  psindctl_out_data_update(mask, state);

  return 0;
}

/*
 * check returns
 *     0: link up and OK
 *    -1: link down
 *    -2: link switched to down
 *     1: link up,but not reinitialized
 *     2: link switched to up
 */
void
psindctl_past_net_state_led_callback(int netst)
{
  uint32_t mask;
  uint32_t state = 0;

  mask = PSINDCTL_LED_ETH_G_m | PSINDCTL_LED_ETH_Y_m |
         PSINDCTL_LED_S2_R_m | PSINDCTL_LED_S2_G_m;

  switch (netst) {
    case 2:
    case 1:
      state |= PSINDCTL_LED_ETH_Y_m | PSINDCTL_LED_S2_R_m;
    case 0:
      state |= PSINDCTL_LED_ETH_G_m | PSINDCTL_LED_S2_G_m;
      break;
    case -1:
      break;
    default:
      state |= PSINDCTL_LED_ETH_Y_m | PSINDCTL_LED_S2_R_m;
  }
  psindctl_out_data_update(mask, state);
}

void psindctl_past_pwr_stop(void)
{
  psindctl_out_data_update(PSINDCTL_PWR_ON_ENABLE_m, 0);
}

void psindctl_past_pwr_enable(void)
{
  psindctl_out_data_update(PSINDCTL_PWR_ON_ENABLE_m,
                           PSINDCTL_PWR_ON_ENABLE_m);
}
