/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  psindctl_spi.c - power supply and indicators control

  (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <spi_drv.h>
#include <inttypes.h>
#include <string.h>

#include "psindctl_spi.h"

#define PSINDCTL_SPI_NUMBER   1

#define PSINDCTL_PWR_ON_PULSE_CONTROL
#define PSINDCTL_PWR_ON_PULSE_WIDTH 20

static int
psindctl_spi_transfer_prepare(psindctl_spi_state_t *psctlst)
{
  uint32_t dout = psctlst->data_out;

 #ifdef PSINDCTL_PWR_ON_PULSE_CONTROL
  if (psctlst->pwr_on_pulse_cnt) {
    psctlst->pwr_on_pulse_cnt--;
    dout |= PSINDCTL_PWR_ON_PULSE_m;
  } else {
    dout &= ~PSINDCTL_PWR_ON_PULSE_m;
  }
 #endif /*PSINDCTL_PWR_ON_PULSE_CONTROL*/

  memcpy(psctlst->tx_buff, &dout, 4);

  psctlst->rq_pend = 0;

  if(spi_msg_rq_ins(psctlst->spi_drv, &psctlst->spi_msg) < 0) {
    psctlst->msg_inpr = -1;
    return -1;
  }

  return 1;
}

static int
psindctl_spi_transfer_callback(struct spi_drv *drv, int code, struct spi_msg_head *msg)
{
  psindctl_spi_state_t *psctlst = UL_CONTAINEROF(msg, psindctl_spi_state_t, spi_msg);
  uint32_t din;

  memcpy(&din, psctlst->rx_buff, 4);
  psctlst->data_in_old = psctlst->data_in;
  psctlst->data_in = din;

  psctlst->msg_inpr = 0;

  if (!psctlst->rq_pend)
    return 1;

  psindctl_spi_transfer_prepare(psctlst);

  return 1;
}

int
psindctl_spi_state_setup(psindctl_spi_state_t *psctlst, spi_drv_t *spi_drv)
{
  psctlst->spi_msg.flags = SPI_MSG_MODE_SET;
  psctlst->spi_msg.size_mode = SPI_MODE_3;
  psctlst->spi_msg.addr = 4;
  psctlst->spi_msg.rq_len = 4;
  psctlst->spi_msg.tx_buf = psctlst->tx_buff;
  psctlst->spi_msg.rx_buf = psctlst->rx_buff;
  psctlst->spi_msg.callback = psindctl_spi_transfer_callback;
  psctlst->spi_msg.private = 0;

  psctlst->spi_drv = spi_drv;

  return 0;
}

static int
psindctl_spi_transfer_request(psindctl_spi_state_t *psctlst)
{
  unsigned long flags;
  int ready_fl;

  save_and_cli(flags);
  ready_fl = !psctlst->msg_inpr;
  if (!psctlst->msg_inpr) {
    psctlst->msg_inpr = 1;
  } else {
    psctlst->rq_pend = 1;
  }
  restore_flags(flags);

  if (!ready_fl)
    return 0;

  return psindctl_spi_transfer_prepare(psctlst);
}

/*******************************************************************/

int psindctl_spi_initialized;

psindctl_spi_state_t psindctl_spi_state;

int
psindctl_spi_init(void)
{
  spi_drv_t *spi_drv;
  psindctl_spi_state_t *psctlst = &psindctl_spi_state;

  if (psindctl_spi_initialized <= 0) {

    spi_drv = spi_find_drv(NULL, PSINDCTL_SPI_NUMBER);
    if (spi_drv == NULL)
      return -1;

    psindctl_spi_state_setup(psctlst, spi_drv);

    psindctl_spi_initialized = 1;
  }

  return 0;
}

int
psindctl_spi_send_rq(void)
{
  psindctl_spi_state_t *psctlst = &psindctl_spi_state;
  if ((psctlst->spi_drv == NULL) || (psindctl_spi_initialized != 1))
    return -1;
  return psindctl_spi_transfer_request(psctlst);
}

uint32_t psindctl_in_data_get(void)
{
  psindctl_spi_state_t *psctlst = &psindctl_spi_state;
  return psctlst->data_in;
}

uint32_t psindctl_out_data_update(uint32_t mask, uint32_t xor_state)
{
  unsigned long flags;
  uint32_t prev_data;
  psindctl_spi_state_t *psctlst = &psindctl_spi_state;

  save_and_cli(flags);
  prev_data = psctlst->data_out;
  psctlst->data_out = (prev_data & ~mask) ^ xor_state;
  restore_flags(flags);

  return prev_data;
}

#ifdef PSINDCTL_PWR_ON_PULSE_CONTROL
void psindctl_initiate_pwr_on_pulse(void)
{
  psindctl_spi_state_t *psctlst = &psindctl_spi_state;
  psctlst->pwr_on_pulse_cnt = PSINDCTL_PWR_ON_PULSE_WIDTH;
}
#endif /*PSINDCTL_PWR_ON_PULSE_CONTROL*/
