/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  psindctl_past.c - ROCON power control and status

  (C) 2001-2016 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2016 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PSINDCTL_PAST_H
#define _PSINDCTL_PAST_H

int psindctl_past_init(void);
int psindctl_past_poll(void);
void psindctl_past_net_state_led_callback(int netst);
void psindctl_past_pwr_stop(void);
void psindctl_past_pwr_enable(void);

#endif /*_PSINDCTL_PAST_H*/
