#include <system_def.h>
#include <stdio.h>

#include <lt_timer.h>

#include "appl_defs.h"

#ifndef APPL_WITH_SIM_POSIX
#include <hal_machperiph.h>
#include <hal_gpio.h>
#endif /*APPL_WITH_SIM_POSIX*/

#ifdef APPL_WITH_PSINDCTL_PAST
#include "psindctl_past.h"
#endif /*APPL_WITH_PSINDCTL_PAST*/

int app_exit_request;

int timer_led_usbact_off;
int timer_led_usbconfigured;
int timer_str;

void timer_10ms(void)
{
#ifndef APPL_WITH_SIM_POSIX

  if (timer_led_usbact_off != 0)
    timer_led_usbact_off--;
  else
    hal_gpio_set_value(LED2_PIN, 1);

#ifdef APPL_WITH_ULAN

  if (!timer_str)
  {
    timer_str = 10;
    ul_stroke(ul_fd);
  }
  else
    timer_str--;

#endif /*APPL_WITH_ULAN*/

  if (timer_led_usbconfigured != 0)
    timer_led_usbconfigured--;
  else
  {
    timer_led_usbconfigured = 20;

    if (1 /*!ul_dcnv_is_open(&ep1_dcnv_state)*/)
    {
      if (hal_gpio_get_value(LED1_PIN))
        hal_gpio_set_value(LED1_PIN, 0);
      else
        hal_gpio_set_value(LED1_PIN, 1);
    }
  }

#endif /*APPL_WITH_SIM_POSIX*/
}


void mloop()
{
  lt_mstime_t ltime;

  lt_mstime_t led1_time;
  lt_mstime_t led2_time;

  lt_mstime_update();
  ltime = actual_msec;
  led1_time = actual_msec;
  led2_time = actual_msec;

  while (!app_exit_request)
  {

    /* 10ms timer */
    if (lt_10msec_expired(10))
    {
      timer_10ms();

#ifdef CONFIG_OC_MWENGINE
      appl_update_indicators();
#endif /*CONFIG_OC_MWENGINE*/

#ifdef WATCHDOG_ENABLED
      watchdog_feed();
#endif /* WATCHDOG_ENABLED */
    }

#ifdef APPL_WITH_ULAN

    if (ul_inepoll(ul_fd) > 0)
    {
      int free_fl = 0;

      do
      {
        /* processing of ulan messages */
        if (ul_acceptmsg(ul_fd, &umsginfo) < 0)
          break;  /* No mesage reported - break */

        free_fl = 1;

        if (umsginfo.flg & (UL_BFL_PROC | UL_BFL_FAIL))
          break;  /* Reported message informs about fault or carried out processing */

        if (umsginfo.cmd == uloi_con_ulan_cmd(coninfo))
        {
          if (uloi_process_msg(ULOI_ARG_coninfo(uloi_objdes_array_t *)&uloi_objdes_main, &umsginfo) >= 0)
            free_fl = 0;

          break;
        }

#ifdef CONFIG_ULAN_DY

        if (umsginfo.cmd == UL_CMD_NCS)
        {
          if (uldy_process_msg(ULDY_ARG_ul_dyac & umsginfo) >= 0)
            free_fl = 0;

          break;
        }

#endif /*CONFIG_ULAN_DY*/

      }
      while (0);

      if (free_fl)
        ul_freemsg(ul_fd);
    }

#ifdef CONFIG_ULAN_DY

    /* test request for address */
    if (uldy_rqa(ULDY_ARG1_ul_dyac))
      uldy_addr_rq(ULDY_ARG1_ul_dyac);

#endif /*CONFIG_ULAN_DY*/
#endif /*APPL_WITH_ULAN*/

    lt_mstime_update();

    if ((actual_msec - ltime) > 5000)
      ltime += 5000;

#ifdef APPL_RUN_AT_MAIN_LOOP
    APPL_RUN_AT_MAIN_LOOP;
#endif /*APPL_RUN_AT_MAIN_LOOP*/

#ifdef CONFIG_OC_MWENGINE
    gui_poll();
#endif /*CONFIG_OC_MWENGINE*/

#ifdef APPL_WITH_USB

    if (usb_enable_flag)
    {
      if (usb_app_poll() > 0)
      {
        timer_led_usbact_off = 5;
        hal_gpio_set_value(LED2_PIN, 0);
      }
    }

#endif /*APPL_WITH_USB*/

#ifdef CONFIG_OC_CMDPROC
    cmdproc_poll();
#endif /*CONFIG_OC_CMDPROC*/

#ifdef APPL_WITH_DISTORE_EEPROM_USER
    appl_distore_user_change_check();
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

#ifdef APPL_WITH_LWIP
    lwip_app_poll();
#endif /*CONFIG_APP_ROCON_WITH_LWIP*/

#ifdef APPL_WITH_PSINDCTL_PAST
    psindctl_past_poll();
#endif /*APPL_WITH_PSINDCTL_PAST*/
  }
}
