/*******************************************************************
  Motion and Robotic System (MARS) aplication components

  appl_dprint.c - debug print to the console

  Copyright (C) 2001-2014 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2014 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <ctype.h>
#include <string.h>
#include <pxmc.h>
#include <pxmc_coordmv.h>
#include <cmd_proc.h>
#include <pxmc_cmds.h>
#include <utils.h>

#include "appl_defs.h"
#include "appl_dprint.h"

#ifndef APPL_WITH_DPRINT_PER_CMDIO
unsigned dbg_prt_flg=0;
#endif /*APPL_WITH_DPRINT_PER_CMDIO*/

/* selection of debug messages */

#define	DBGPF_AXES	0x00ff	/* mask for all axes */
#define	DBGPF_PER_TIME	0x0100	/* periodic time print */
#define	DBGPF_PER_POS	0x0800	/* periodic position info */
#define	DBGPF_CMD_PROC	0x1000	/* command proccessing */
#define	DBGPF_PER_CUR	0x2000	/* periodic current info */

typedef struct dbg_prt_des{
  unsigned mask;
  char *name;
}dbg_prt_des_t;


const dbg_prt_des_t dbg_prt_des[]={
  {~0, "all"},
  {0x01, "A"}, {0x02, "B"}, {0x04, "C"}, {0x08, "D"},
  {0x10, "E"}, {0x20, "F"}, {0x40, "G"}, {0x80, "H"},
  {DBGPF_PER_TIME, "time"},
  {DBGPF_PER_POS,  "pos"},
  {DBGPF_CMD_PROC, "cmd"},
  {DBGPF_PER_CUR,  "cur"},
  {0,NULL}
};

void run_dbg_prt_flg(cmd_io_t *cmd_io, unsigned flg)
{
  char s[20];
  int i;
  pxmc_state_t *mcs;

  if(flg & (DBGPF_PER_POS | DBGPF_PER_CUR)) {
    char reg_ascii_id[4];
    short reg_mask;
    reg_ascii_id[1]='!';
    reg_ascii_id[2]=0;

    for(i = 0, reg_mask = 1; i < pxmc_main_list.pxml_cnt; i++, reg_mask <<= 1) {
      if(flg & reg_mask & DBGPF_AXES){
	reg_ascii_id[0]='A'+i;
        cmd_io_puts(cmd_io,reg_ascii_id);
        mcs=pxmc_main_list.pxml_arr[i];
        i2str(s,mcs->pxms_ap>>PXMC_SUBDIV(mcs),8,0);
        cmd_io_puts(cmd_io,s);
        cmd_io_puts(cmd_io,mcs->pxms_flg&PXMS_ERR_m?" E":
	    		      mcs->pxms_flg&PXMS_BSY_m?" B":" -");

        if (flg & DBGPF_PER_POS) {
          cmd_io_puts(cmd_io," hal");
          i2str(s,mcs->pxms_hal,2,0);
          cmd_io_puts(cmd_io,s);

          cmd_io_puts(cmd_io,mcs->pxms_flg&PXMS_PHA_m?" A":
	    		      mcs->pxms_flg&PXMS_PTI_m?" I":" -");

          cmd_io_puts(cmd_io," i");
          i2str(s,mcs->pxms_ptindx,4,0);
          cmd_io_puts(cmd_io,s);

          cmd_io_puts(cmd_io," o");
          i2str(s,mcs->pxms_ptofs,6,0);
          cmd_io_puts(cmd_io,s);
        }

       #ifdef PXMC_WITH_EXTENDED_STATE
        cmd_io_puts(cmd_io," ene_d");
        i2str(s,mcs->pxms_ene_d,7,0);
        cmd_io_puts(cmd_io,s);
       #endif /*PXMC_WITH_EXTENDED_STATE*/

        cmd_io_puts(cmd_io," ene");
        i2str(s,mcs->pxms_ene,7,0);
        cmd_io_puts(cmd_io,s);

       #ifdef PXMC_WITH_EXTENDED_STATE
        if (flg & DBGPF_PER_CUR) {
          long cur_d = mcs->pxms_cur_d_act;
          long cur_q = mcs->pxms_cur_q_act;
          long long cur_d2;
          long long cur_q2;
          __memory_barrier();
          cur_d2 = (long long)cur_d * cur_d;
          cur_q2 = (long long)cur_q * cur_q;

          cmd_io_puts(cmd_io," cur");
          i2str(s,sqrtll(cur_d2 + cur_q2),7,0);
          cmd_io_puts(cmd_io,s);

          cmd_io_puts(cmd_io," d");
          i2str(s,cur_d,7,0);
          cmd_io_puts(cmd_io,s);

          cmd_io_puts(cmd_io," q");
          i2str(s,cur_q,7,0);
          cmd_io_puts(cmd_io,s);
        }
       #endif /*PXMC_WITH_EXTENDED_STATE*/

        cmd_io_puts(cmd_io,"\r\n");
      }
    }
  }
}

#ifndef APPL_WITH_DPRINT_PER_CMDIO
void run_dbg_prt(cmd_io_t *cmd_io)
{
  run_dbg_prt_flg(cmd_io, dbg_prt_flg);
}
#endif /*APPL_WITH_DPRINT_PER_CMDIO*/

int cmd_do_switches_at_ptr(cmd_io_t *cmd_io, int mode, unsigned *pval,
                           const dbg_prt_des_t *pdes, char *param[])
{
  unsigned val;
  long l;
  const dbg_prt_des_t *pd;
  char *ps;
  char str[20];
  char pm_flag;
  ps=(mode&CDESM_OPCHR)?param[3]:param[1];
  val=*pval;
  if(*ps=='?'){
    while(pdes->name){
      cmd_io_puts(cmd_io, " ");
      cmd_io_puts(cmd_io, pdes->name);
      pdes++;
    }
  }
  while(*ps){
    si_skspace(&ps);
    if(!*ps) break;
    pm_flag=0;
    if(*ps=='+'){ps++;}
    else if(*ps=='-'){pm_flag=1;ps++;}
    else val=0;
    if(isdigit((uint8_t)*ps)){if(si_long(&ps,&l,0)<0) return -1;}
    else{
      si_alnumn(&ps,str,20);
      pd=pdes;
      do{
        if(!pd->name) return -1;
	if(!strcmp(pd->name,str)){
	  l=pd->mask; break;
	}
	pd++;
      }while(1);
    }
    if(pm_flag) val&=~l;
    else val|=l;
  }
  *pval=val;
  return 0;
}

int cmd_do_switches(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned *pval;
  dbg_prt_des_t *pdes;
  pval = (unsigned*)(des->info[0]);
  pdes = (dbg_prt_des_t*)(des->info[1]);

  return cmd_do_switches_at_ptr(cmd_io, des->mode, pval, pdes, param);
}

#ifdef APPL_WITH_DPRINT_PER_CMDIO

int cmd_do_dprint(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned *pval;
  dbg_prt_des_t *pdes;
  pval = cmdproc_dbg_prt_flg_for_cmd_io(cmd_io);
  if (pval == NULL)
    return -1;
  pdes = (dbg_prt_des_t*)(des->info[0]);
  return cmd_do_switches_at_ptr(cmd_io, des->mode, pval, pdes, param);
}

cmd_des_t const cmd_des_dprint={0, 0,"dprint","enable debug messages to print, use + - to add remove, list types ?",
			cmd_do_dprint,
			{(char*)dbg_prt_des}};

#else /*APPL_WITH_DPRINT_PER_CMDIO*/
cmd_des_t const cmd_des_dprint={0, 0,"dprint","enable debug messages to print, use + - to add remove, list types ?",
			cmd_do_switches,
			{(char*)&dbg_prt_flg,(char*)dbg_prt_des}};
#endif /*APPL_WITH_DPRINT_PER_CMDIO*/

