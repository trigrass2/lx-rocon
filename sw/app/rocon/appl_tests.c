#include <system_def.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <malloc.h>
#include <utils.h>
#include <cmd_proc.h>
#include <hal_gpio.h>
#include <hal_machperiph.h>
#include <LPC17xx.h>
#include <lpcTIM.h>
#include <spi_drv.h>
#include <pxmc.h>
#include <inttypes.h>

#include <ul_log.h>
#include <ul_logreg.h>

#include "appl_defs.h"
#include "appl_fpga.h"
#include "appl_pxmc.h"
#include "pxmcc_types.h"
#include "pxmcc_interface.h"
#include "as5_spi.h"

#ifdef CONFIG_OC_MTD_DRV_SYSLESS
#include <mtd_spi_drv.h>
#endif
#ifdef APPL_WITH_PSINDCTL
#include "psindctl_spi.h"
#endif /*APPL_WITH_PSINDCTL*/

int cmd_do_test_memusage(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  void *maxaddr;
  char str[40];

  maxaddr = sbrk(0);

  snprintf(str, sizeof(str), "memusage maxaddr 0x%08lx\n", (unsigned long)maxaddr);
  cmd_io_write(cmd_io, str, strlen(str));

  return 0;
}

int cmd_do_test_adc(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  FILE *F;

  if (cmd_io->priv.ed_line.io_stack)
    cmd_io = cmd_io->priv.ed_line.io_stack;

  F = cmd_io_as_file(cmd_io, "r+");
  if (F == NULL)
    return CMDERR_EIO;

  fprintf(F, "ADC: %ld %ld %ld %ld %ld\n", (LPC_ADC->DR[0] & 0xFFF0) >> 4,
         (LPC_ADC->DR[1] & 0xFFF0) >> 4,
         (LPC_ADC->DR[2] & 0xFFF0) >> 4,
         (LPC_ADC->DR[3] & 0xFFF0) >> 4,
         (LPC_ADC->DR[7] & 0xFFF0) >> 4);
  fclose(F);
  return 0;
}

#ifdef APPL_WITH_DISTORE_EEPROM_USER
int cmd_do_test_distore(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  appl_distore_user_set_check4change();
  return 0;
}

int cmd_do_test_diload(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  appl_distore_user_restore();
  return 0;
}
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

int cmd_do_test_loglevel_cb(ul_log_domain_t *domain, void *context)
{
  char s[30];
  cmd_io_t *cmd_io = (cmd_io_t *)context;

  s[sizeof(s) - 1] = 0;
  snprintf(s, sizeof(s) - 1, "%s (%d)\n\r", domain->name, domain->level);
  cmd_io_puts(cmd_io, s);
  return 0;
}

int cmd_do_test_loglevel(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res = 0;
  char *line;
  line = param[1];

  if (!line || (si_skspace(&line), !*line))
  {
    ul_logreg_for_each_domain(cmd_do_test_loglevel_cb, cmd_io);
  }
  else
  {
    res = ul_log_domain_arg2levels(line);
  }

  return res >= 0 ? 0 : CMDERR_BADPAR;
}

int cmd_do_spimst_blocking(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res;
  int opchar;
  char *p = param[3];
  int spi_chan = (int)(intptr_t)des->info[0];
  uint8_t *tx_buff = NULL;
  uint8_t *rx_buff = NULL;
  long addr = 0;
  int len = 0;
  spi_drv_t *spi_drv;
  FILE *F;

  if ((opchar = cmd_opchar_check(cmd_io, des, param)) < 0)
    return opchar;

  if (opchar != ':')
    return -CMDERR_OPCHAR;

  if (spi_chan == -1)
    spi_chan = *param[1] - '0';

  spi_drv = spi_find_drv(NULL, spi_chan);

  if (spi_drv == NULL)
    return -CMDERR_BADSUF;

  p = param[3];

  si_skspace(&p);

  if (isdigit((int)*p))
  {
    if (si_long(&p, &addr, 16) < 0)
      return -CMDERR_BADPAR;
  }

  if (si_fndsep(&p, "({") < 0)
    return -CMDERR_BADSEP;

  if ((res = si_add_to_arr(&p, (void **)&tx_buff, &len, 16, 1, "})")) < 0)
    return -CMDERR_BADPAR;

  rx_buff = malloc(len);

  res = -1;

  if (rx_buff != NULL)
    res = spi_transfer(spi_drv, addr, len, tx_buff, rx_buff);

  if (cmd_io->priv.ed_line.io_stack)
    cmd_io = cmd_io->priv.ed_line.io_stack;

  F = cmd_io_as_file(cmd_io, "r+");
  if (F == NULL)
    return CMDERR_EIO;

  if (res < 0)
  {
    fprintf(F, "SPI! %02lX ERROR\n", addr);
  }
  else
  {
    int i;
    fprintf(F, "SPI! %02lX ", addr);
    fprintf(F, "TX(");

    for (i = 0; i < len; i++)
      fprintf(F, "%s%02X", i ? "," : "", tx_buff[i]);

    fprintf(F, ") RX(");

    for (i = 0; i < len; i++)
      fprintf(F, "%s%02X", i ? "," : "", rx_buff[i]);

    fprintf(F, ")");
    fprintf(F, "\n");
  }

  if (rx_buff)
    free(rx_buff);

  if (tx_buff)
    free(tx_buff);

  fclose(F);

  return 0;
}

int cmd_do_mtdspitest(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res;
  unsigned char id_buff[3];
  FILE *F;

  if (cmd_io->priv.ed_line.io_stack)
    cmd_io = cmd_io->priv.ed_line.io_stack;

  F = cmd_io_as_file(cmd_io, "r+");
  if (F == NULL)
    return CMDERR_EIO;

  res = mtd_spi_read_jedec_id(&mtd_spi_state, id_buff, 3);
  if (res < 0) {
    fprintf(F, "mtd_spi_read_jedec_id returned %d\n", res);
    goto error;
  }

  fprintf(F, "mtd_spi_read_jedec_id 0x%02x 0x%02x 0x%02x \n",
         id_buff[0], id_buff[1], id_buff[2]);

  res = mtd_spi_set_protect_mode(&mtd_spi_state, 0, 0);
  if (res < 0) {
    fprintf(F, "mtd_spi_set_protect_mode returned %d\n", res);
    goto error;
  }

  fclose(F);
  return 0;

error:
  fclose(F);
  return -1;
}

#ifdef SDRAM_BASE
int sdram_access_test(void)
{
  unsigned int *ptr;
  unsigned int pattern;
  size_t ramsz = SDRAM_SIZE;
  size_t cnt;
  lt_mstime_t tic;
  size_t blksz, i;

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;

  for (cnt = ramsz / sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;)
  {
    *(ptr++) = pattern;
    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM write %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;

  for (cnt = ramsz / sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;)
  {
    if (*ptr != pattern)
    {
      printf("SDRAM error modify at %p (%08x)\n", ptr, *ptr ^ pattern);
      return -1;
    }

    *(ptr++) = ~pattern;
    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM modify %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0x12abcdef;

  for (cnt = ramsz / sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;)
  {
    if (*(ptr++) != ~pattern)
    {
      printf("SDRAM error read at %p (%08x)\n", ptr, *ptr ^ pattern);
      return -1;
    }

    pattern = pattern + 0x87654321;
  }

  lt_mstime_update();
  printf("SDRAM read %d ms\n", (int)(lt_msdiff_t)(actual_msec - tic));

  lt_mstime_update();
  tic = actual_msec;

  pattern = 0;

  for (cnt = ramsz / sizeof(*ptr), ptr = (typeof(ptr))SDRAM_BASE; cnt--;)
  {
    pattern += *(ptr++);
  }

  lt_mstime_update();
  printf("SDRAM sum %d ms res 0x%08x\n", (int)(lt_msdiff_t)(actual_msec - tic), pattern);

  for (blksz = 1; blksz < 256 ; blksz *= 2)
  {
    lt_mstime_update();
    tic = actual_msec;

    pattern = 0;

    for (cnt = ramsz / sizeof(*ptr); cnt; cnt -= blksz)
    {
      ptr = (typeof(ptr))SDRAM_BASE;

      //ptr = (typeof(ptr))cmd_do_test_memusage;
      //ptr = (typeof(ptr))&ptr;
      for (i = blksz; i--;)
        pattern += *(ptr++);
    }

    lt_mstime_update();
    printf("SDRAM sum %d blksz %d ms res 0x%08x\n", (int)(lt_msdiff_t)(actual_msec - tic), (int)blksz, pattern);
  }

  return 0;
}

int cmd_do_testsdram(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  sdram_access_test();
  return 0;
}
#endif /*SDRAM_BASE*/

int cmd_do_testlxpwrrx(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps = param[1];
  long mode = 0;
  uint32_t *ptr;

  if (ps != NULL) {
    si_skspace(&ps);
    if (*ps) {
      if (si_ulong(&ps, &mode, 0) < 0)
        return -CMDERR_BADPAR;
    }
  }
  pxmc_rocon_rx_data_hist_buff = NULL;
  pxmc_rocon_rx_data_hist_mode = mode;

 #ifndef PXMC_ROCON_TIMED_BY_RX_DONE
  pxmc_rocon_rx_done_isr_setup(pxmc_rocon_rx_done_isr);
 #endif /*PXMC_ROCON_TIMED_BY_RX_DONE*/
  pxmc_rocon_rx_data_hist_buff_end = (void *)(FPGA_CONFIGURATION_FILE_ADDRESS +
                                         0x80000);
  ptr = (void *)FPGA_CONFIGURATION_FILE_ADDRESS;
  if (mode != 0) {
    *(ptr++) = '10XL';
    *(ptr++) = mode;
  }
  pxmc_rocon_rx_data_hist_buff = (void *)ptr;
  return 0;
}

int cmd_do_testlxpwrstat(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char str[50];
  snprintf(str, sizeof(str), "lxpwrrx period %ld latency %ld max %ld",
         (long)pxmc_rocon_rx_cycle_time, (long)pxmc_rocon_rx_irq_latency,
         (long)pxmc_rocon_rx_irq_latency_max);
  cmd_io_write(cmd_io, str, strlen(str));
  return 0;
}

#include <math.h>

int cmd_do_testfncapprox(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps = param[1];
  unsigned long fnc;
  unsigned long val;
  unsigned long res;
  long diff;
  long diff_max = 0;
  unsigned int  xb;
  uint32_t x;
  uint32_t xl;
  double   xf;
  int64_t  yl;
  long count = 1;
  long step = 1 << (30-7-18+1  +4);

  si_skspace(&ps);
  if (si_ulong(&ps, &fnc, 0) < 0)
      return -CMDERR_BADPAR;

  si_skspace(&ps);
  if (!strcmp(ps, "all")) {
    val = 0;
    count = 0x80000000UL / step;
    if (fnc == 1) {
      val = 1;
    }
  } else {
    if (si_ulong(&ps, &val, 0) < 0)
        return -CMDERR_BADPAR;
  }

  for (; count--;  val += step) {

    if (fnc == 1) {
      x = val;
      xb = __builtin_clz(x);
      xl = x << xb;
    } else {
      xl = val;
    }

    fpga_fncapprox_base[fnc] = xl;

    /* dummy read to provide time to function aproximator to proceed computation */
    res = fpga_fncapprox_base[fnc];
    res = fpga_fncapprox_base[fnc];

    switch (fnc) {
      case 0:
        yl = xl;
      case 1:
        yl = (1LL << 62) / xl;
        break;
      case 2:
        xf = (double)xl * M_PI / 2.0 / (1UL << 30);
        yl = round(sin(xf) * (1UL << 16));
        break;
      case 3:
        xf = (double)xl * M_PI / 2.0 / (1UL << 30);
        yl = round(cos(xf) * (1UL << 16));
        break;
      default:
      yl = 0;
    }

    diff = yl - res;

    if ((diff > 0) && (diff > diff_max))
      diff_max = diff;
    else if ((diff < 0) && (-diff > diff_max))
      diff_max = -diff;

  }

  val -= step;

  printf("fnc=%ld val=0x%08lx res=0x%08lx ref=0x%08lx diff=%ld max %ld\n",
         fnc, val, res, (unsigned long)yl, diff, diff_max);

  return 0;
}

int cmd_do_testtumblefw(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps = param[1];
  long pwm_d;
  long pwm_q;
  pxmc_state_t *mcs = pxmc_main_list.pxml_arr[0];
  volatile pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();

  pxmcc_axis_enable(mcs, 0);

  si_skspace(&ps);
  if (si_long(&ps, &pwm_d, 0) < 0)
      return -CMDERR_BADPAR;

  si_skspace(&ps);
  if (si_ulong(&ps, &pwm_q, 0) < 0)
        return -CMDERR_BADPAR;


  if (0) {
    pxmcc_axis_setup(mcs, PXMCC_MODE_BLDC);
  }
  pxmc_clear_flags(mcs,PXMS_ENO_m|PXMS_ENG_m|PXMS_ENR_m|PXMS_BSY_m);
  pxmcc_axis_pwm_dq_out(mcs, pwm_d, pwm_q);
  pxmcc_axis_enable(mcs, 1);

  if (0) {
    mcc_data->axis[1].inp_info = 0;
    mcc_data->axis[1].out_info = 3;
    mcc_data->axis[1].pwmtx_info = (12 << 0) | (13 << 8) | (14 << 16);
    mcc_data->axis[1].mode = PXMCC_MODE_BLDC;
    mcc_data->axis[1].ccflg = 1;
    mcc_data->axis[2].inp_info = 0;
    mcc_data->axis[2].out_info = 6;
    mcc_data->axis[2].pwmtx_info = (15 << 0) | (16 << 8) | (18 << 16);
    mcc_data->axis[2].mode = PXMCC_MODE_BLDC;
    mcc_data->axis[2].ccflg = 1;
    mcc_data->axis[3].inp_info = 0;
    mcc_data->axis[3].out_info = 9;
    mcc_data->axis[3].pwmtx_info = (19 << 0) | (20 << 8) | (21 << 16);
    mcc_data->axis[3].mode = PXMCC_MODE_BLDC;
    mcc_data->axis[3].ccflg = 1;
  }

  printf("spd %ld act_idle %"PRIu32" min_idle %"PRIu32" avail %lu pwm_cycle %"PRIu32"\n",
         mcs->pxms_as, mcc_data->common.act_idle, mcc_data->common.min_idle,
         (mcc_data->common.pwm_cycle + 6) / 6, mcc_data->common.pwm_cycle);
  mcc_data->common.min_idle = 0x7fff;

  return 0;
}

#define CK_IRC_WORDS 16
#define CK_TX_WORDS  16
#define CK_RX_WORDS  16

#define CK_IRC_START ((uint32_t*)fpga_irc[0])
#define CK_TX_START  (fpga_lx_master_transmitter_base+9)
#define CK_RX_START  (fpga_lx_master_receiver_base+44)

typedef struct ck_state_t {
  uint32_t ck_irc_base[CK_IRC_WORDS];
  uint32_t ck_irc_read[CK_IRC_WORDS];
  uint32_t ck_tx_base[CK_TX_WORDS];
  uint32_t ck_tx_read[CK_TX_WORDS];
  uint32_t ck_rx_base[CK_RX_WORDS];
  uint32_t ck_rx_read[CK_RX_WORDS];

  uint32_t ck_irc_err;
  uint32_t ck_tx_err;
  uint32_t ck_rx_err;
} ck_state_t;

int cmd_do_testtumblebus(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int i;
  int cycles = 10000;
  static ck_state_t *ckst = NULL;
  if (ckst == NULL)
    ckst = malloc(sizeof(*ckst));
  if (ckst == NULL)
    return 1;

  ckst->ck_irc_err = 0;
  ckst->ck_tx_err = 0;
  ckst->ck_rx_err = 0;

  for (i = 0; i < CK_IRC_WORDS; i++)
    ckst->ck_irc_base[i] = CK_IRC_START[i];

  for (i = 0; i < CK_TX_WORDS; i++)
    ckst->ck_tx_base[i] = CK_TX_START[i];

  for (i = 0; i < CK_RX_WORDS; i++)
    ckst->ck_rx_base[i] = CK_RX_START[i];

  while (cycles--) {
    if (!ckst->ck_irc_err) {
      for (i = 0; i < CK_IRC_WORDS; i++)
        ckst->ck_irc_read[i] = CK_IRC_START[i];
      for (i = 0; i < CK_IRC_WORDS; i++)
        if (ckst->ck_irc_read[i] != ckst->ck_irc_base[i]) {
          ckst->ck_irc_err++;
          printf("irc+%x %08"PRIx32" != %08"PRIx32"\n",
                 i, ckst->ck_irc_read[i], ckst->ck_irc_base[i]);
        }
    }

    if (!ckst->ck_tx_err) {
      for (i = 0; i < CK_TX_WORDS; i++)
        ckst->ck_tx_read[i] = CK_TX_START[i];
      for (i = 0; i < CK_TX_WORDS; i++)
        if (ckst->ck_tx_read[i] != ckst->ck_tx_base[i]) {
          ckst->ck_tx_err++;
          printf("tx+%x %08"PRIx32" != %08"PRIx32"\n",
                 i, ckst->ck_tx_read[i], ckst->ck_tx_base[i]);
        }
    }

    if (!ckst->ck_rx_err) {
      for (i = 0; i < CK_RX_WORDS; i++)
        ckst->ck_rx_read[i] = CK_RX_START[i];
      for (i = 0; i < CK_RX_WORDS; i++)
        if (ckst->ck_rx_read[i] != ckst->ck_rx_base[i]) {
          ckst->ck_rx_err++;
          printf("rx+%x %08"PRIx32" != %08"PRIx32"\n",
                 i, ckst->ck_rx_read[i], ckst->ck_rx_base[i]);
        }
    }
  }

  return 0;
}

typedef struct {
  const char *name;
  unsigned offset;
} text_pxmcc_fileds_des_t;

static const text_pxmcc_fileds_des_t text_pxmcc_fileds_des[] = {
  {"ccflg", pxmcc_axis_data_offs(ccflg)},
  {"mode", pxmcc_axis_data_offs(mode)},
  {"pwm_dq", pxmcc_axis_data_offs(pwm_dq)},
  {"cur_dq", pxmcc_axis_data_offs(cur_dq)},
  {"ptindx", pxmcc_axis_data_offs(ptindx)},
  {"ptirc", pxmcc_axis_data_offs(ptirc)},
  {"ptreci", pxmcc_axis_data_offs(ptreci)},
  {"ptofs", pxmcc_axis_data_offs(ptofs)},
  {"ptsin", pxmcc_axis_data_offs(ptsin)},
  {"ptcos", pxmcc_axis_data_offs(ptcos)},
  {"ptphs", pxmcc_axis_data_offs(ptphs)},
  {"cur_d_cum", pxmcc_axis_data_offs(cur_d_cum)},
  {"cur_q_cum", pxmcc_axis_data_offs(cur_q_cum)},
  {"inp_info", pxmcc_axis_data_offs(inp_info)},
  {"out_info", pxmcc_axis_data_offs(out_info)},
  {"pwmtx_info", pxmcc_axis_data_offs(pwmtx_info)},
  {"pwm_prew[0]", pxmcc_axis_data_offs(pwm_prew[0])},
  {"pwm_prew[1]", pxmcc_axis_data_offs(pwm_prew[1])},
  {"pwm_prew[2]", pxmcc_axis_data_offs(pwm_prew[2])},
  {"pwm_prew[3]", pxmcc_axis_data_offs(pwm_prew[3])},
  {"steps_inc", pxmcc_axis_data_offs(steps_inc)},
  {"steps_pos", pxmcc_axis_data_offs(steps_pos)},
  {"steps_sqn_next", pxmcc_axis_data_offs(steps_sqn_next)},
  {"steps_inc_next", pxmcc_axis_data_offs(steps_inc_next)},
  {"steps_pos_next", pxmcc_axis_data_offs(steps_pos_next)},
  {NULL, 0},
};

int cmd_do_testpxmccstate(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps = param[1];
  pxmc_state_t *mcs = pxmc_main_list.pxml_arr[0];
  volatile pxmcc_axis_data_t *mcc_axis;
  text_pxmcc_fileds_des_t *pdes;

  si_skspace(&ps);

  if (mcs == NULL)
    return -CMDERR_NODEV;

  mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);

  printf("irc 0x%08x\n", pxmc_rocon_read_irc(mcs->pxms_inp_info));
  printf("pxms_ap 0x%08lx\n", mcs->pxms_ap);
  printf("pxms_ptofs 0x%08x\n", mcs->pxms_ptofs);
  printf("pxms_ptmark 0x%08x\n", mcs->pxms_ptmark);

  for (pdes = text_pxmcc_fileds_des; pdes->name; pdes++) {
    printf("%s 0x%08"PRIx32"\n", pdes->name,
            *(volatile uint32_t *)((volatile char*)mcc_axis + pdes->offset));
  }

  return 0;
}


int cmd_do_testcuradc(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps = param[1];
  long pwm_chan_a;
  long pwm_chan_b;
  long pwm_b = 0;
  pxmc_state_t *mcs;
  volatile pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();
  int i;
  long cur_a;
  long cur_b;
  long pwm_cycle = mcc_data->common.pwm_cycle;
  char str[80];


  si_skspace(&ps);
  if (si_long(&ps, &pwm_chan_a, 0) < 0)
      return -CMDERR_BADPAR;

  si_skspace(&ps);
  if (si_ulong(&ps, &pwm_chan_b, 0) < 0)
        return -CMDERR_BADPAR;

  if (pwm_chan_a >= PXMCC_CURADC_CHANNELS)
    return -CMDERR_BADPAR;

  if (pwm_chan_b >= PXMCC_CURADC_CHANNELS)
    return -CMDERR_BADPAR;

  si_skspace(&ps);
  if (*ps) {
    if (si_ulong(&ps, &pwm_b, 0) < 0)
          return -CMDERR_BADPAR;
    pxmc_for_each_mcs(i, mcs) {
      /* PXMS_ENI_m - check if input (IRC) update is enabled */
      if (mcs->pxms_flg & (PXMS_ENR_m | PXMS_ENO_m)) {
        pxmc_axis_release(mcs);
      }
    }

    for (i = 0; i < 16; i++) {
      if (i == pwm_chan_a) {
        if (pxmc_rocon_pwm_direct_wr(i, 0, 1) < 0)
          return -CMDERR_EIO;
      } else if (i == pwm_chan_b) {
        if (pxmc_rocon_pwm_direct_wr(i, pwm_b, 1) < 0)
          return -CMDERR_EIO;
      } else {
        pxmc_rocon_pwm_direct_wr(i, 0, 0);
      }
    }
  }

  cur_a = mcc_data->curadc[pwm_chan_a].cur_val;
  cur_b = mcc_data->curadc[pwm_chan_b].cur_val;
  if (pwm_b < pwm_cycle)
    cur_b = (pwm_cycle * cur_b + pwm_cycle / 2) / (pwm_cycle - pwm_b);

  snprintf(str, sizeof(str), "chA %2ld pwm %5ld cur %7ld chB %2ld pwm %5ld cur %7ld",
         pwm_chan_a, 0l, cur_a, pwm_chan_b, pwm_b, cur_b);

  cmd_io_write(cmd_io, str, strlen(str));

  return 0;
}

int cmd_do_curadc(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int chan;
  volatile pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();
  volatile pxmcc_curadc_data_t *curadc;
  int subcmd = (int)des->info[0];
  char str[40] = "";

  for (chan = 0; chan < PXMCC_CURADC_CHANNELS; chan++) {
    curadc = mcc_data->curadc + chan;
    switch (subcmd) {
      case 0:
        snprintf(str, sizeof(str), "%s%ld", chan?",":"", (long)curadc->cur_val);
        cmd_io_write(cmd_io, str, strlen(str));
        break;
      case 1:
        snprintf(str, sizeof(str), "%s%ld", chan?",":"", (long)curadc->siroladc_offs);
        cmd_io_write(cmd_io, str, strlen(str));
        break;
      case 2:
        curadc->siroladc_offs += curadc->cur_val;
        break;
    }
  }

  return 0;
}

int cmd_do_testas5(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  char *ps = param[1];
  as5_spi_state_t *as5st = &as5_spi_state;
  as5_chan_state_t *as5chst = &as5st->chan[0];
  unsigned int pos;
  char str[70] = "";

  if (as5_spi_init() < 0)
    return -1;

  if (ps != NULL) {
    si_skspace(&ps);
    if (!strncmp(ps, "diag", 4)) {
      as5_spi_add2rq_mask(as5st, 0, AS5_SPI_MASK_CLRERR |
                     AS5_SPI_MASK_RDMAG | AS5_SPI_MASK_RDDIAG);
    }
  }

  pos = as5_spi_get_pos(0);
  snprintf(str, sizeof(str), "as5 pos 0x%04x mag 0x%04x diag 0x%04x err 0x%01x cnt %d",
         pos, as5chst->mag, as5chst->diag, as5chst->err, as5chst->err_cnt);
  cmd_io_write(cmd_io, str, strlen(str));

  as5_spi_pos_update();

  return 0;
}

#ifdef APPL_WITH_PSINDCTL
int cmd_do_testpsindctl(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  unsigned long l;
  char *ps = param[1];
  char str[40];

  if (ps != NULL) {
    si_skspace(&ps);
    if (*ps) {
      if (si_ulong(&ps, &l, 0) < 0)
        return -CMDERR_BADPAR;

      psindctl_spi_state.data_out = l;
    }
  }

  if (psindctl_spi_init() < 0)
    return -CMDERR_EIO;

  snprintf(str, sizeof(str), "out %08"PRIx32" in %08"PRIx32,
     psindctl_spi_state.data_out, psindctl_spi_state.data_in);
  cmd_io_write(cmd_io, str, strlen(str));

  psindctl_spi_send_rq();

  return 0;
}
#endif /*APPL_WITH_PSINDCTL*/

cmd_des_t const cmd_des_test_memusage = {0, 0,
                "memusage", "report memory usage", cmd_do_test_memusage,
{
  0,
  0
}
                                        };

cmd_des_t const cmd_des_test_adc = {0, 0,
                                    "testadc", "adc test", cmd_do_test_adc,
{
  0,
  0
}
                                   };

#ifdef APPL_WITH_DISTORE_EEPROM_USER
cmd_des_t const cmd_des_test_distore = {0, 0,
                                        "testdistore", "test DINFO store", cmd_do_test_distore,
{
  0,
  0
}
                                       };

cmd_des_t const cmd_des_test_diload = {0, 0,
                                       "testdiload", "test DINFO load", cmd_do_test_diload,
{
  0,
  0
}
                                      };
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

cmd_des_t const cmd_des_test_loglevel = {0, 0,
                "loglevel", "select logging level",
                cmd_do_test_loglevel, {}
                                        };

cmd_des_t const cmd_des_spimst = {0, CDESM_OPCHR | CDESM_WR,
                                  "SPIMST", "SPI master communication request",
                                  cmd_do_spimst_blocking, {(void *)0}
                                 };

cmd_des_t const cmd_des_spimstx = {0, CDESM_OPCHR | CDESM_WR,
                                   "SPIMST#", "SPI# master communication request",
                                   cmd_do_spimst_blocking, {(void *) - 1}
                                  };

cmd_des_t const cmd_des_mtdspitest = {0, 0,
                                   "mtdspitest", "test SPI connected Flash",
                                   cmd_do_mtdspitest, {(void *) - 1}
                                  };

#ifdef SDRAM_BASE
cmd_des_t const cmd_des_testsdram = {0, 0,
                                     "testsdram", "test SDRAM",
                                     cmd_do_testsdram, {(void *)0}
                                    };
#endif /*SDRAM_BASE*/


cmd_des_t const cmd_des_testlxpwrrx = {0, 0,
                                     "testlxpwrrx", "capture data stream from lxpwr",
                                     cmd_do_testlxpwrrx, {(void *)0}
                                    };

cmd_des_t const cmd_des_testlxpwrstat = {0, 0,
                                     "testlxpwrstat", "lxpwr interrupt statistic",
                                     cmd_do_testlxpwrstat, {(void *)0}
                                    };

cmd_des_t const cmd_des_testfncapprox = {0, 0,
                                     "testfncapprox", "test of function approximator",
                                     cmd_do_testfncapprox, {(void *)0}
                                    };

cmd_des_t const cmd_des_testtumblefw = {0, 0,
                                     "testtumblefw", "test Tumble coprocesor firmware",
                                     cmd_do_testtumblefw, {(void *)0}
                                    };

cmd_des_t const cmd_des_testtumblebus = {0, 0,
                                     "testtumblebus", "test Tumble coprocesor bus",
                                     cmd_do_testtumblebus, {(void *)0}
                                    };

cmd_des_t const cmd_des_testpxmccstate = {0, 0,
                                     "testpxmccstate", "test to display pxmccstate",
                                     cmd_do_testpxmccstate, {(void *)0}
                                    };

cmd_des_t const cmd_des_testcuradc = {0, 0,
                                     "testcuradc", "test current adc channel calibration",
                                     cmd_do_testcuradc, {(void *)0}
                                    };

cmd_des_t const cmd_des_showcuradc = {0, 0,
                                     "showcuradc", "print current ADC offsets",
                                     cmd_do_curadc, {(void *)0}
                                    };

cmd_des_t const cmd_des_showcuradcoffs = {0, 0,
                                     "showcuradcoffs", "print current ADC offsets",
                                     cmd_do_curadc, {(void *)1}
                                    };

cmd_des_t const cmd_des_calcuradcoffs = {0, 0,
                                     "calcuradcoffs", "calibrate current ADC offsets",
                                     cmd_do_curadc, {(void *)2}
                                    };

cmd_des_t const cmd_des_testas5 = {0, 0,
                                     "testas5", "test AMS AS5048 sensor",
                                     cmd_do_testas5, {(void *)0}
                                    };

#ifdef APPL_WITH_PSINDCTL
cmd_des_t const cmd_des_testpsindctl = {0, 0,
                                     "testpsindctl", "test power sources indication and control",
                                     cmd_do_testpsindctl, {(void *)0}
                                    };
#endif /*APPL_WITH_PSINDCTL*/

cmd_des_t const *const cmd_appl_tests[] =
{
  &cmd_des_test_memusage,
  &cmd_des_test_adc,
#ifdef APPL_WITH_DISTORE_EEPROM_USER
  &cmd_des_test_distore,
  &cmd_des_test_diload,
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/
  &cmd_des_test_loglevel,
  &cmd_des_spimst,
  &cmd_des_spimstx,
  &cmd_des_mtdspitest,
#ifdef SDRAM_BASE
  &cmd_des_testsdram,
#endif /*SDRAM_BASE*/
  &cmd_des_testlxpwrrx,
  &cmd_des_testlxpwrstat,
  &cmd_des_testfncapprox,
  &cmd_des_testtumblefw,
  &cmd_des_testtumblebus,
  &cmd_des_testpxmccstate,
  &cmd_des_testcuradc,
  &cmd_des_showcuradc,
  &cmd_des_showcuradcoffs,
  &cmd_des_calcuradcoffs,
  &cmd_des_testas5,
#ifdef APPL_WITH_PSINDCTL
  &cmd_des_testpsindctl,
#endif /*APPL_WITH_PSINDCTL*/
  NULL
};
