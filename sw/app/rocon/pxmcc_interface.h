/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  pxmcc_interface.h - multi axis motion controller coprocessor
               interface for access lx-rocon system

  (C) 2001-2014 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2014 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PXMCC_INTERFACE_H_
#define _PXMCC_INTERFACE_H_

#include <stdint.h>
#include <pxmc.h>
#include "pxmcc_types.h"

/* evaluate offset of field from beginning of pxmcc_axis_data_t */
#define pxmcc_axis_data_offs(_fld) \
                ((size_t)&((pxmcc_axis_data_t *)0L)->_fld)

static inline
pxmcc_data_t *pxmc_rocon_mcc_data(void)
{
  return (pxmcc_data_t *)fpga_tumbl_dmem;
}

static inline
pxmcc_axis_data_t *pxmc_rocon_mcs2pxmcc(pxmc_state_t *mcs)
{
  pxmcc_data_t *mcc_data = pxmc_rocon_mcc_data();

  if (mcs->pxms_inp_info >= PXMCC_AXIS_COUNT)
    return NULL;

  return mcc_data->axis + mcs->pxms_inp_info;
}

static inline
void pxmcc_axis_pwm_dq_out(pxmc_state_t *mcs, int pwm_d, int pwm_q)
{
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);
  mcc_axis->pwm_dq = (pwm_d << 16) | (pwm_q & 0xffff);
}

static inline
void pxmcc_axis_enable(pxmc_state_t *mcs, int enable)
{
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);
  mcc_axis->ccflg = enable? 1: 0;
}

static inline
void pxmcc_axis_get_cur_dq_act(pxmc_state_t *mcs, int *p_cur_d, int *p_cur_q)
{
  volatile pxmcc_axis_data_t *mcc_axis = pxmc_rocon_mcs2pxmcc(mcs);
  uint32_t cur_dq = mcc_axis->cur_dq;
  *p_cur_d = (int16_t)(cur_dq >> 16);
  *p_cur_q = (int16_t)(cur_dq);
}

void pxmcc_axis_get_cur_dq_filt(pxmc_state_t *mcs, int *p_cur_d, int *p_cur_q);

void pxmcc_pxmc_ptofs2mcc(pxmc_state_t *mcs, int enable_update);

int pxmcc_axis_setup(pxmc_state_t *mcs, int mode);

#endif /*_PXMCC_INTERFACE_H_*/
