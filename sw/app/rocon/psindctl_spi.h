/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  psindctl_spi.h - power supply and indicators control

  (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _PSINDCTL_SPI_H
#define _PSINDCTL_SPI_H

#include <spi_drv.h>
#include <stdint.h>

/* BIT 0 Red A */
#define PSINDCTL_LED_AXIS_X_R_b(ax) ((ax) * 2)
/* BIT 1 Ggreen A */
#define PSINDCTL_LED_AXIS_X_G_b(ax) ((ax) * 2 + 1)

/* BIT 16 Led ERR SW */
#define PSINDCTL_LED_STOP_SW_b      16
/* BIT 17 Led ARM SW */
#define PSINDCTL_LED_ARM_SW_b       17
/* BIT 18 Led ETH */
#define PSINDCTL_LED_ETH_G_b        18
/* BIT 19 Led ETH */
#define PSINDCTL_LED_ETH_Y_b        19
/* BIT 20 Enable PWR ON */
#define PSINDCTL_PWR_ON_ENABLE_b    20
/* BIT 22 PWR ON pulse */
#define PSINDCTL_PWR_ON_PULSE_b     22

/* BIT 24 Err LED Red */
#define PSINDCTL_LED_ERR_R_b        24
/* BIT 25 Err LED Green */
#define PSINDCTL_LED_ERR_G_b        25
/* BIT 26 Busy LED Red */
#define PSINDCTL_LED_BUSY_R_b       26
/* BIT 27 Busy LED Green */
#define PSINDCTL_LED_BUSY_G_b       27

/* BIT 28 S2 LED Red */
#define PSINDCTL_LED_S2_R_b         28
/* BIT 29 S2 LED Green */
#define PSINDCTL_LED_S2_G_b         29
/* BIT 30 S1 LED Red */
#define PSINDCTL_LED_S1_R_b         30
/* BIT 31 S1 LED Green */
#define PSINDCTL_LED_S1_G_b         31

#define PSINDCTL_LED_AXIS_X_R_m(ax) \
        (1 << PSINDCTL_LED_AXIS_X_R_b(ax))
#define PSINDCTL_LED_AXIS_X_G_m(ax) \
        (1 << PSINDCTL_LED_AXIS_X_G_b(ax))

#define PSINDCTL_LED_STOP_SW_m \
        (1 << PSINDCTL_LED_STOP_SW_b)
#define PSINDCTL_LED_ARM_SW_m \
        (1 << PSINDCTL_LED_ARM_SW_b)
#define PSINDCTL_LED_ETH_G_m \
        (1 << PSINDCTL_LED_ETH_G_b)
#define PSINDCTL_LED_ETH_Y_m \
        (1 << PSINDCTL_LED_ETH_Y_b)
#define PSINDCTL_PWR_ON_ENABLE_m \
        (1 << PSINDCTL_PWR_ON_ENABLE_b)
#define PSINDCTL_PWR_ON_PULSE_m \
        (1 << PSINDCTL_PWR_ON_PULSE_b)
#define PSINDCTL_LED_ERR_R_m \
        (1 << PSINDCTL_LED_ERR_R_b)
#define PSINDCTL_LED_ERR_G_m \
        (1 << PSINDCTL_LED_ERR_G_b)
#define PSINDCTL_LED_BUSY_R_m \
        (1 << PSINDCTL_LED_BUSY_R_b)
#define PSINDCTL_LED_BUSY_G_m \
        (1 << PSINDCTL_LED_BUSY_G_b)
#define PSINDCTL_LED_S2_R_m \
        (1 << PSINDCTL_LED_S2_R_b)
#define PSINDCTL_LED_S2_G_m \
        (1 << PSINDCTL_LED_S2_G_b)
#define PSINDCTL_LED_S1_R_m \
        (1 << PSINDCTL_LED_S1_R_b)
#define PSINDCTL_LED_S1_G_m \
        (1 << PSINDCTL_LED_S1_G_b)

/* Read word */

/* Bit 1 Arm SW */
#define PSINDCTL_SW_ARM_b     1
/* Bit 2 STOP SW */
#define PSINDCTL_SW_STOP_b    2
/* Bit 3 Safety SW */
#define PSINDCTL_SW_SAFETY_b  3
/* Bit 4 Relay */
#define PSINDCTL_SW_RELAY_b   4

#define PSINDCTL_SW_FORCED1_b 0
#define PSINDCTL_SW_FORCED0_b 7

#define PSINDCTL_SW_ARM_m \
        (1 << PSINDCTL_SW_ARM_b)
#define PSINDCTL_SW_STOP_m \
        (1 << PSINDCTL_SW_STOP_b)
#define PSINDCTL_SW_SAFETY_m \
        (1 << PSINDCTL_SW_SAFETY_b)
#define PSINDCTL_SW_RELAY_m \
        (1 << PSINDCTL_SW_RELAY_b)

#define PSINDCTL_SW_FORCED1_m \
        (1 << PSINDCTL_SW_FORCED1_b)
#define PSINDCTL_SW_FORCED0_m \
        (1 << PSINDCTL_SW_FORCED0_b)

typedef struct {
  spi_drv_t *spi_drv;
  spi_msg_head_t spi_msg;
  unsigned char tx_buff[4];
  unsigned char rx_buff[4];
  volatile char msg_inpr;
  volatile char rq_pend;
  uint32_t data_out;
  uint32_t data_in;
  uint32_t data_in_old;
  unsigned int pwr_on_pulse_cnt;
} psindctl_spi_state_t;

int psindctl_spi_init(void);
int psindctl_spi_send_rq(void);
uint32_t psindctl_in_data_get(void);
uint32_t psindctl_out_data_update(uint32_t mask, uint32_t xor_state);
void psindctl_initiate_pwr_on_pulse(void);

extern psindctl_spi_state_t psindctl_spi_state;

#endif /*_PSINDCTL_SPI_H*/
