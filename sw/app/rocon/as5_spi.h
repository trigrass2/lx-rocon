/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  as5_spi.h - AMS AS5048 HAL based angular sensor

  (C) 2001-2015 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2015 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#ifndef _AS5_SPI_H
#define _AS5_SPI_H


#include <spi_drv.h>
#include <inttypes.h>

#define AS5_SPI_REG_RDANGLE 0xffff
#define AS5_SPI_REG_RDMAG   0x7ffe
#define AS5_SPI_REG_RDDIAG  0x7ffd
#define AS5_SPI_REG_CLRERR  0x4001

#define AS5_SPI_REPLY_ERR   0x4000
#define AS5_SPI_REPLY_PAR   0x8000

#define AS5_SPI_CMD_RDANGLE  1
#define AS5_SPI_CMD_RDMAG    2
#define AS5_SPI_CMD_RDDIAG   3
#define AS5_SPI_CMD_CLRERR   4

#define AS5_SPI_MASK_CLRERR  0x01
#define AS5_SPI_MASK_RDMAG   0x02
#define AS5_SPI_MASK_RDDIAG  0x04

typedef struct {
  uint16_t pos;
  uint16_t mag;
  uint16_t diag;
  uint8_t  err;
  uint8_t  rq_mask;
  uint16_t err_cnt;
  uint8_t  act_cmd;
  uint8_t  prev_cmd;
} as5_chan_state_t;

typedef struct {
  spi_drv_t *spi_drv;
  spi_msg_head_t spi_msg;
  unsigned char tx_buff[2];
  unsigned char rx_buff[2];
  volatile char msg_inpr;
  volatile char rq_pend;
  volatile char diag_pend;
  unsigned char chan_num;
  unsigned char chan_cnt;
  as5_chan_state_t *chan;
  unsigned int  *chan_addr;
} as5_spi_state_t;

int as5_spi_add2rq_mask(as5_spi_state_t *as5st, int chan, unsigned int rq_mask);

int as5_spi_init(void);
int as5_spi_pos_update(void);
unsigned int as5_spi_get_pos(int chan);

extern as5_spi_state_t as5_spi_state;

#endif /*_AS5_SPI_H*/
