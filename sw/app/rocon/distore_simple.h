#ifndef _DISTORE_SIMPLE_H
#define _DISTORE_SIMPLE_H

#include <string.h>
#include <suiut/sui_dinfo.h>

#include "appl_defs.h"

#ifdef CONFIG_OC_I2C_DRV_SYSLESS
#include <i2c_drv.h>
extern i2c_drv_t i2c_drv;
#endif

enum distore_encformat
{
  DISTORE_ENC_END           = 0x00,
  DISTORE_ENC_FLOAT_CODE    = 0x10,
  DISTORE_ENC_SIGNED_CODE   = 0x20,
  DISTORE_ENC_UNSIGNED_CODE = 0x30,
  DISTORE_ENC_NUM_LEN       = 0x0f,
  DISTORE_ENC_NUM_LEN_BITS  =    4,
  DISTORE_ENC_TEXT_CODE     = 0x80,
  DISTORE_ENC_TEXT_LEN      = 0x7f,
};

#define DISTORE_FLG_LOAD_ONLY 1

typedef unsigned short distore_id_t;

typedef struct distore_des_item_t
{
  sui_dinfo_t *dinfo;
  unsigned char encform;
  unsigned char idxlimit;
  distore_id_t id;
  unsigned short flags;
} distore_des_item_t;

typedef struct distore_des_array_t
{
  struct
  {
    const distore_des_item_t *items;
    unsigned count;
  } array;
} distore_des_array_t;

const distore_des_item_t *
distore_des_array_at(const distore_des_array_t *array, unsigned indx);

const distore_des_item_t *
distore_des_array_find(const distore_des_array_t *array,
                       distore_id_t const *key);

typedef ssize_t distore_wrfnc_t(void *context, const void *buf, size_t count);
typedef ssize_t distore_rdfnc_t(void *context, void *buf, size_t count);

ssize_t distore_count_maxsize(const distore_des_array_t *des);

int distore_store_data(const distore_des_array_t *des,
                       distore_wrfnc_t *wrfnc, void *context);

#define DISTORE_LOAD_IGNORE_UNKNOWN     1
#define DISTORE_LOAD_IGNORE_WRITE_ERR   2

int distore_load_data(const distore_des_array_t *des,
                      distore_rdfnc_t *rdfnc, void *context, int mode);

#endif /* _DISTORE_SIMPLE_H*/
