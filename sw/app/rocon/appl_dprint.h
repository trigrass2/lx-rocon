#ifndef _APPL_DPRINT_H
#define _APPL_DPRINT_H

extern cmd_des_t const cmd_des_dprint;
void run_dbg_prt(cmd_io_t *cmd_io);
void run_dbg_prt_flg(cmd_io_t *cmd_io, unsigned flg);
int cmd_do_dprint(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[]);
unsigned *cmdproc_dbg_prt_flg_for_cmd_io(cmd_io_t *cmd_io);

#endif /*_APPL_DPRINT_H*/
