#include <inttypes.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <stdlib.h>
#include <cmd_proc.h>
#include <ser_drv.h>
#include <lpcuart.h>

#include "appl_defs.h"
#include "appl_fpga.h"
#include "appl_version.h"


#ifdef CONFIG_OC_CMDPROC

cmd_io_t cmd_io_uart1con_dev;

#define ED_LINE_CHARS 512

char ed_line_chars_uart1con_in[ED_LINE_CHARS + 1];
char ed_line_chars_uart1con_out[ED_LINE_CHARS + 1];

ed_line_buf_t ed_line_buf_uart1con_in =
{
flg:
  FL_ELB_ECHO,
  inbuf: 0,
alloc:
  sizeof(ed_line_chars_uart1con_in),
  maxlen: 0,
  lastch: 0,
buf:
  ed_line_chars_uart1con_in
};

ed_line_buf_t ed_line_buf_uart1con_out =
{
flg:
  FL_ELB_NOCRLF,
  inbuf: 0,
alloc:
  sizeof(ed_line_chars_uart1con_out),
  maxlen: 0,
  lastch: 0,
buf:
  ed_line_chars_uart1con_out
};

cmd_io_t cmd_io_uart1con =
{
putc:
  cmd_io_line_putc,
getc:
  NULL,
write:
  cmd_io_write_bychar,
read:
  NULL,
priv:
  {
  ed_line:
    {
    in:
      &ed_line_buf_uart1con_in,
    out:
      &ed_line_buf_uart1con_out,
    io_stack:
      &cmd_io_uart1con_dev
    }
  }
};

#endif


#define LPC_SER_DRV_UART1_BUF_SIZE (1024 * 10)

LPC_SER_DRV_STATE_DEFINE_BUFFERS_PROVIDED(1, 115200, 0, SER_DRV_FLOWC_HW_m, NULL, 0, NULL, 0)

int uart1_app_init(void)
{
  ser_drv_t *drvst = LPC_SER_DRV_STATE_PTR(1);
  void *buf_in;
  void *buf_out;

  buf_in = malloc(LPC_SER_DRV_UART1_BUF_SIZE);
  buf_out = malloc(LPC_SER_DRV_UART1_BUF_SIZE);
  if ((buf_in == NULL) || (buf_out == NULL))
    goto error;
  ser_drv_setup_buffers(drvst, buf_in, LPC_SER_DRV_UART1_BUF_SIZE,
                                 buf_out, LPC_SER_DRV_UART1_BUF_SIZE);

  if (ser_drv_set_init_state(drvst) < 0)
    goto error;

 #ifdef CONFIG_OC_CMDPROC
  {
    char str[100];
    snprintf(str, sizeof(str), "\n\r#%s v%d.%d.%d\n\r",
               APP_VER_ID, APP_VER_MAJOR, APP_VER_MINOR, APP_VER_PATCH);
    cmd_io_write(&cmd_io_uart1con, str, strlen(str));
  }
 #endif
  return 0;

 error:
  return -1;
}


#ifdef CONFIG_OC_CMDPROC

int cmd_io_getc_uart1con(struct cmd_io *cmd_io)
{
  ser_drv_t *drvst = LPC_SER_DRV_STATE_PTR(1);
  return ser_drv_rec_chr(drvst);
}

int cmd_io_putc_uart1con(struct cmd_io *cmd_io, int ch)
{
  ser_drv_t *drvst = LPC_SER_DRV_STATE_PTR(1);
  return ser_drv_send_chr(drvst, ch);
}

cmd_io_t cmd_io_uart1con_dev =
{
  .putc = cmd_io_putc_uart1con,
  .getc = cmd_io_getc_uart1con,
  .write = cmd_io_write_bychar,
  .read = cmd_io_read_bychar,
  .priv.uart = { 1 }
};

#endif
