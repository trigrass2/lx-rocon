#include <string.h>
#include <stdint.h>

#include "appl_defs.h"

#include "appl_eeprom.h"

#ifdef CONFIG_OC_I2C_DRV_SYSLESS
#include <i2c_drv.h>
extern i2c_drv_t i2c_drv;
#endif

#include "distore_simple.h"

#include <ul_log.h>
extern UL_LOG_CUST(ulogd_distore)

#ifdef APPL_WITH_TIMEPROG_EEPROM_STORE
#include "timeprog.h"
extern  timeprog_t appl_timeprog_eeprom_identifier;
#endif

#ifdef CONFIG_OC_I2C_DRV_SYSLESS

void *(*const appl_distore_reserve_ram)(size_t size);

typedef struct appl_distore_eeprom_context_t
{
  size_t pos;
  size_t limit;
  void *buf;
  const distore_des_array_t *des;
  int changed: 1;
  int write2nd: 1;
  volatile unsigned char check4change;
  unsigned char gen_cnt;
  unsigned short area_start;
  unsigned short area_size;

  appl_eeprom_chip_context_t *chip;
} appl_distore_eeprom_context_t;

ssize_t appl_distore_eeprom_wrfnc(void *context, const void *buf, size_t count)
{
  appl_distore_eeprom_context_t *c = (appl_distore_eeprom_context_t *) context;

  if (c->limit - c->pos < count)
    return -1;

  if (memcmp((char *) c->buf + c->pos, buf, count))
    c->changed = 1;

  memcpy((char *) c->buf + c->pos, buf, count);
  c->pos += count;
  return count;
}

ssize_t appl_distore_eeprom_rdfnc(void *context, void *buf, size_t count)
{
  appl_distore_eeprom_context_t *c = (appl_distore_eeprom_context_t *) context;

  if (c->limit - c->pos < count)
    return -1;

  memcpy(buf, (char *) c->buf + c->pos, count);
  c->pos += count;
  return count;
}

unsigned int appl_distore_eeprom_chksum(void *data, size_t size)
{
  unsigned int chksum = 0x1234;
  unsigned char *p = data;

  while (size --)
  {
    chksum = (*p ^ chksum) + (*p << 8) + 1;
    p++;
  }

  return chksum;
}

int appl_distore_eeprom_load(appl_distore_eeprom_context_t *c)
{
  const distore_des_array_t *des = c->des;
  unsigned char u[DISTORE_EEPROM_HEADER_SIZE];
  size_t ma;
  unsigned int chksum, load_chksum;
  int res;
  unsigned char area_bad[2] = {0, 0};
  unsigned char area_gen_cnt[2];
  unsigned int area_limit[2];
  int area_act;

  if (c->buf == NULL)
    return -1;

  if (c->chip == NULL)
    return -1;

  for (area_act = 0; area_act < 2; area_act++)
  {
    ma = c->area_start + c->area_size * area_act;

    if (appl_eeprom_chip_load(c->chip, ma, u, DISTORE_EEPROM_HEADER_SIZE) < 0)
    {
      return -1;
    }

    if (u[0] != DISTORE_EEPROM_SIGNATURE)
    {
      area_bad[area_act] = 1;
      ul_logmsg("appl_distore_eeprom_load area %d has bad signature\n", area_act);
      continue;
    }

    area_gen_cnt[area_act] = u[1];
    area_limit[area_act] = u[4] | ((unsigned int) u[5] << 8);

    if (c->area_size < area_limit[area_act])
    {
      area_bad[area_act] = 1;
      ul_logmsg("appl_distore_eeprom_load area %d size 0x%lx < stored size 0x%lx\n", area_act,
                (unsigned long) c->area_size, (unsigned long) area_limit[area_act]);
      continue;
    }

    ul_logdeb("appl_distore_eeprom_load area %d gen_cnt is 0x%x stored size 0x%lx\n",
              area_act, u[1], (unsigned long) area_limit[area_act]);
  }

  area_act = 0;

  if ((ul_cyclic_gt(area_gen_cnt[1], area_gen_cnt[0]) && !area_bad[1]) || area_bad[0])
    area_act = 1;

  for (; 1; area_bad[area_act] = 1, area_act = area_act ^ 1)
  {
    if (area_bad[area_act])
    {
      ul_logmsg("appl_distore_eeprom_load no valid area found\n");
      return -1;
    }

    c->limit = area_limit[area_act];

    ma = c->area_start + c->area_size * area_act;

    if (appl_eeprom_chip_load(c->chip, ma, c->buf, c->limit) < 0)
    {
      continue;
    }

    chksum = appl_distore_eeprom_chksum((unsigned char *) c->buf + 4, c->limit - 4);
    load_chksum = * ((unsigned char *) c->buf + 2);
    load_chksum |= * ((unsigned char *) c->buf + 3) << 8;

    if ((chksum ^ load_chksum) & 0xffff)
    {
      ul_logmsg("appl_distore_eeprom_load area %d has bad checksum (0x%04x != 0x%04x)\n",
                area_act, chksum & 0xffff, load_chksum);
      continue;
    }

    c->write2nd = area_act ^ 1;
    c->gen_cnt = area_gen_cnt[area_act];
    ul_logdeb("appl_distore_eeprom_load reading data from %d area\n", area_act);

    c->pos = DISTORE_EEPROM_HEADER_SIZE;

    res = distore_load_data(des, appl_distore_eeprom_rdfnc, c,
                            DISTORE_LOAD_IGNORE_UNKNOWN | DISTORE_LOAD_IGNORE_WRITE_ERR);

    if (res < 0)
    {
      ul_logmsg("appl_distore_eeprom_load data decode from %d area failed\n", area_act);
      continue;
    }
    else
    {
      ul_logmsg("appl_distore_eeprom_load decoded from %d area\n", area_act);
    }

    break;
  }

  return res;
}

int appl_distore_eeprom_init(appl_distore_eeprom_context_t *c)
{
  const distore_des_array_t *des = c->des;
  ssize_t sz;

  c->buf = NULL;
  c->limit = 0;
  c->pos = 0;
  sz = distore_count_maxsize(des);

  if (sz < 0)
    return -1;

  if (sz + DISTORE_EEPROM_HEADER_SIZE >= c->area_size)
  {
    ul_logerr("appl_distore max_size %ld > EEPROM target area %ld\n",
              (unsigned long) sz, (unsigned long) c->area_size);
    return -1;
  }

  if (appl_distore_reserve_ram != NULL)
    c->buf = appl_distore_reserve_ram(c->area_size);

  if (c->buf == NULL)
    c->buf = malloc(c->area_size);

  if (c->buf == NULL)
  {
    ul_logerr("appl_distore_eeprom_init RAM allocation failed\n");
    return -1;
  }

  memset(c->buf, 0, c->area_size > 16 ? 16 : c->area_size);

  c->limit = c->area_size;

  return 0;
}

int appl_distore_eeprom_store(appl_distore_eeprom_context_t *c, int forced)
{
  int res;
  unsigned int chksum;
  unsigned char u[DISTORE_EEPROM_HEADER_SIZE];
  const distore_des_array_t *des = c->des;

  if (c->buf == NULL)
    return -1;

  if (c->chip->tx_inpr > 0)
    return -2;

  c->check4change = 0;

  c->pos = DISTORE_EEPROM_HEADER_SIZE;
  c->limit = c->area_size;

  c->changed = 0;

  res = distore_store_data(des, appl_distore_eeprom_wrfnc, c);

  if (res < 0)
  {
    return -1;
  }

  if (!c->changed && !forced)
    return 0;

  c->limit = c->pos;

  /* The length of the data is part of the area covered by checksum */
  u[4] = c->limit & 0xff;
  u[5] = c->limit >> 8;
  c->pos = 4;
  appl_distore_eeprom_wrfnc(c, u + 4, 2);

  chksum = appl_distore_eeprom_chksum((unsigned char *) c->buf + 4, c->limit - 4);

  u[0] = DISTORE_EEPROM_SIGNATURE;
  u[1] = (c->gen_cnt + 1) & 0xff;
  u[2] = chksum & 0xff;
  u[3] = (chksum >> 8) & 0xff;

  c->pos = 0;
  appl_distore_eeprom_wrfnc(c, u, DISTORE_EEPROM_HEADER_SIZE);

  c->pos = 0;

  return 1;
}

#endif /*CONFIG_OC_I2C_DRV_SYSLESS*/

#ifdef APPL_WITH_DISTORE_EEPROM_USER
extern const distore_des_array_t appl_distore_eeprom_user_des;

appl_eeprom_chip_context_t appl_eeprom_chip_context =
{
  .page_size = DISTORE_EEPROM_PAGE,
  .i2c_drv = &i2c_drv,
  .i2c_addr = DISTORE_EEPROM_I2C_ADDR,
};

appl_distore_eeprom_context_t appl_distore_eeprom_user_context =
{
  .des = &appl_distore_eeprom_user_des,
  .area_start = DISTORE_EEPROM_USER_START,
  .area_size = DISTORE_EEPROM_USER_SIZE,
  .chip = &appl_eeprom_chip_context,
};

int appl_distore_eeprom_user_finish_callback(struct appl_eeprom_chip_context_t *chip,
    void *context, int result)
{
  appl_distore_eeprom_context_t *c = (appl_distore_eeprom_context_t *) context;

  ul_loginf("finished distore save with result %d\n", result);

  if (result >= 0)
  {
    c->write2nd = (c->write2nd ^ 1) & 1;
    c->gen_cnt += 1;
  }

  return 0;
}

#ifdef APPL_WITH_TIMEPROG_EEPROM_STORE
int appl_timeprog_eeprom_store_finish_callback(struct appl_eeprom_chip_context_t *chip,
    void *context, int result)
{
  timeprog_t *timeprog = (timeprog_t *) context;

  ul_loginf("finished timeprog save with result %d\n", result);

  timeprog->save_rq_state = (result >= 0) ? 0 : -1;

  return 0;
}
#endif /*APPL_WITH_TIMEPROG_EEPROM_STORE*/

int appl_distore_user_change_check(void)
{
  int res;
  unsigned int ma;
  appl_distore_eeprom_context_t *c = &appl_distore_eeprom_user_context;
  appl_eeprom_chip_context_t *chip = c->chip;
  int transfer_pend_fl = 0;

  if (chip->tx_inpr > 0)
    return 2;

#ifdef APPL_WITH_TIMEPROG_EEPROM_STORE

  if (chip->opstate == APPL_EEPROM_ST_IDLE)
  {
    timeprog_t *timeprog = &appl_timeprog_eeprom_identifier;

    if (timeprog->save_rq_state)
    {
      long size_to_store = timeprog_size_to_store(timeprog);
      res = -1;

      if ((size_to_store > 0) && (size_to_store <= TIMEPROG_EEPROM_STORE_SIZE))
      {
        res = appl_eeprom_chip_write_transfer_setup(chip, timeprog->region_start,
              TIMEPROG_EEPROM_STORE_START, size_to_store,
              appl_timeprog_eeprom_store_finish_callback, timeprog);
      }

      if (res < 0)
      {
        timeprog->save_rq_state = -1;
      }
      else
      {
        c->check4change = 1;
        transfer_pend_fl = 1;
        timeprog_stored_size = size_to_store;
      }
    }
  }

#endif /*APPL_WITH_TIMEPROG_EEPROM_STORE*/

  if (c->check4change && !transfer_pend_fl)
  {
    res = appl_distore_eeprom_store(c, 0);

    if (res < 0)
      return -1;

    if (res > 0)
    {
      ma = c->area_start;

      if (c->write2nd)
        ma += c->area_size;

      appl_eeprom_chip_write_transfer_setup(chip, c->buf, ma, c->limit,
                                            appl_distore_eeprom_user_finish_callback, c);
    }
  }

  if (chip->opstate == APPL_EEPROM_ST_IDLE)
    return 0;

  return appl_eeprom_chip_process(chip);
}

int appl_distore_user_set_check4change(void)
{
  appl_distore_eeprom_context_t *c = &appl_distore_eeprom_user_context;

  c->check4change = 1;

  return 0;
}

int appl_distore_user_restore(void)
{
  appl_distore_eeprom_context_t *c = &appl_distore_eeprom_user_context;

  return appl_distore_eeprom_load(c);
}

#ifdef APPL_WITH_TIMEPROG_EEPROM_STORE
int appl_timeprog_eeprom_restore(void)
{
  appl_distore_eeprom_context_t *c = &appl_distore_eeprom_user_context;
  appl_eeprom_chip_context_t *chip = c->chip;
  timeprog_t *timeprog = &appl_timeprog_eeprom_identifier;
  long sz = timeprog_stored_size;

  if ((timeprog->region_limit == NULL) || (timeprog->region_start == NULL))
    return -1;

  if ((sz <= 0) || (sz > (char *) timeprog->region_limit - (char *) timeprog->region_start))
    return -1;

  if (sz > TIMEPROG_EEPROM_STORE_SIZE)
    return -1;

  timeprog->preprocess_state = TIMEPROG_PPST_MODIFIED;
  __memory_barrier();

  if (appl_eeprom_chip_load(chip, TIMEPROG_EEPROM_STORE_START, timeprog->region_start, sz) < 0)
  {
    size_t region_size = timeprog->region_limit - timeprog->region_start;
    memset(timeprog->region_start, 0, region_size);
    timeprog->gen_cnt++;
    return -1;
  }

  timeprog->gen_cnt++;

  if (!timeprog_actual_is_empty(timeprog))
    timeprog_end_and_prepare(timeprog);

  return 0;
}
#endif /*APPL_WITH_TIMEPROG_EEPROM_STORE*/


int appl_distore_init(void)
{
  appl_distore_eeprom_context_t *c = &appl_distore_eeprom_user_context;

  if (appl_eeprom_chip_init(c->chip) < 0)
  {
    return -1;
  }

  return appl_distore_eeprom_init(c);
}

#endif /* APPL_WITH_DISTORE_EEPROM_USER */
