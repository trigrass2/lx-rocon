#include <usb.h>
#include <stdio.h>
#include <string.h>

/* ROCON setup */
#define ROCON_VID           0x1669
#define ROCON_PID           0x1023
#define ROCON_CONFIGURATION 1
#define ROCON_INTFERFACE    0

/* Endpoints */
#define EP_IN               0x81
#define EP_OUT              0x01

#define BUFFER_SIZE         1024
#define ANSWER_SIZE         8192

int opening_rocon = 0;

usb_dev_handle *open_dev(void)
{
	struct usb_bus *bus;
	struct usb_device *dev;

	for (bus = usb_get_busses(); bus; bus = bus->next)
	{
		for (dev = bus->devices; dev; dev = dev->next)
		{
			if (dev->descriptor.idVendor == ROCON_VID && dev->descriptor.idProduct == ROCON_PID)
			{
				opening_rocon = 1;
				return usb_open(dev);
			}
		}
	}
	return NULL;
}

int main(int argc, char** argv)
{
	usb_dev_handle *dev = NULL;
	char buffer[BUFFER_SIZE], answer[ANSWER_SIZE];
	int ret, got_ans, i, j;

	/* Init */
	usb_init();
	usb_find_busses();
	usb_find_devices();

	/* Open device */
	if (!(dev = open_dev()))
	{
		if (opening_rocon)
			printf("Error opening ROCON:\n%s\n", usb_strerror());
		else
			printf("ROCON not connected!\n");

		return 1;
	}

	/* Set configuration */
	if (usb_set_configuration(dev, ROCON_CONFIGURATION) < 0)
	{
		printf("Error setting configuration #%d:\n%s\n", ROCON_CONFIGURATION, usb_strerror());
		usb_close(dev);
		return 1;
	}

	/* Set interface */
	if (usb_claim_interface(dev, ROCON_INTFERFACE) < 0)
	{
		printf("Error claiming interface #%d:\n%s\n", ROCON_INTFERFACE, usb_strerror());
		usb_close(dev);
		return 1;
	}

	/* Write command */
	snprintf(buffer, sizeof(buffer), "%s\r\n", argv[1]);
	ret = usb_bulk_write(dev, EP_OUT, buffer, strlen(buffer), 500);
	if (ret < 0)
	{
		printf("Error sending command %s:\n%s\n", argv[1], usb_strerror());
		usb_release_interface(dev, 0);
		usb_close(dev);
		return 1;
	}

	/* Read response */
	got_ans = 0;
	j = 0;

	while (1)
	{
		ret = usb_bulk_read(dev, EP_IN, buffer, sizeof(buffer), 500);
		if (ret < 0)
		{
			if (got_ans)
				break;

			printf("Error reading response:\n%s\n", usb_strerror());
			usb_release_interface(dev, 0);
			usb_close(dev);
			return 1;
		}

		got_ans = 1;
		buffer[ret] = '\0';

		for (i = 0; i < ret; i++)
		{
			if (buffer[i] == '\r')
				continue;

			answer[j] = buffer[i];
			j++;
		}

		/* Two LF => terminator */
		if (j >=2 && answer[j-1] == '\n' && answer[j-2] == '\n')
			break;
	}

	printf(answer);
	usb_release_interface(dev, 0);
	usb_close(dev);
	return 0;
}
 
