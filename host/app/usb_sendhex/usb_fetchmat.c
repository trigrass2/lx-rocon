/* usb_fetchmat - retrieve state from running device in matrix format
 *
 * Based on 'usb_sendhex'
 *
 * Version 1.1 - 2016/07/28
 */
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <usb.h>
#include <sys/time.h>
#include <stdint.h>
#include <endian.h>

#if !defined(_WIN32) && !defined(__DJGPP__) && !defined(HAS_GETDELIM)
#define HAS_GETDELIM
#endif
#define HAS_GETOPT_LONG

#define USB_DEV_VID 0x1669
#define USB_DEV_PID 0x1023

#define USB_TIMEOUT 500

// USB vendor defines

#define USB_VENDOR_GET_CAPABILITIES  0x00 // get capabilities
#define USB_VENDOR_RESET_DEVICE      0x08
#define USB_VENDOR_GET_SET_MEMORY    0x30
#define USB_VENDOR_GET_STATUS        0xF0
#define USB_VENDOR_MASK              0xF8 // mask for vendor commands

#define USB_VENDOR_MEMORY_BY_BULK    0x80

int vid = USB_DEV_VID;
int pid = USB_DEV_PID;
int mem_type   = 4;
unsigned long mem_start  = 0;
unsigned long mem_length = 0;
int mem_length_flg = 0;
int verbose    = 0;
char *file_format = NULL;

typedef struct {
  size_t rows;
  size_t cols;
  int element_type;
  int transpose;
  int endian;
} mat_format_t;

mat_format_t mat_format = {
  .rows = 1,
  .cols = 1,
};

/*****************************************************************************/

#ifndef HAS_GETDELIM
int getdelim(char **line, size_t *linelen, char delim, FILE *F)
{
  char c;
  size_t l = 0;

  do
  {
    if (l + 1 >= *linelen)
    {
      *linelen = l + 20;

      if (!*line)
        *line = (char *)malloc(*linelen);
      else
        *line = (char *)realloc(*line, *linelen);
    }

    c = fgetc(F);

    if (feof(F))
    {
      if (l)
        break;
      else
        return -1;
    }

    if (c != '\r')
      (*line)[l++] = c;
  }
  while (c != delim);

  (*line)[l] = 0;
  return l;
}
#endif


/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/* USB functions */

struct usb_device *find_usb_device(int vendor, int product)
{
  struct usb_bus *bus;
  struct usb_device *dev;

  for (bus = usb_busses; bus; bus = bus->next)
  {
    for (dev = bus->devices; dev; dev = dev->next)
    {
      if ((dev->descriptor.idVendor == vendor) &&
          (dev->descriptor.idProduct) == product)
        return dev;
    }
  }

  return NULL;
}

usb_dev_handle *usb_open_device(int uvid, int upid)
{
  struct usb_device *dev;
  usb_dev_handle *hdev;

  usb_init(); // NO for more devices
  usb_find_busses();
  usb_find_devices();

  dev = find_usb_device(uvid, upid);

  if (!dev)
  {
    if (verbose)
      printf("!!! Cannot find device 0x%04X:0x%04X\n", uvid, upid);

    return NULL;
  }

  if ((hdev = usb_open(dev)) == NULL)
  {
    if (verbose)
      printf("!!! USB device wasn't opened !!!\n");

    return NULL;
  }

  usb_claim_interface(hdev, 0);

  if (verbose)
    printf(" USB Device 0x%04X:0x%04X '%s' is open.\n", uvid, upid, dev->filename);

  return hdev;
}

int usb_close_device(usb_dev_handle *hdev)
{
  int bRes = 1;
  usb_release_interface(hdev, 0);
  bRes = usb_close(hdev);

  if (bRes && verbose)
    printf("!!! USB Device wasn't closed !!!\n");

  return bRes;
}

int upload_matrix(FILE *file, mat_format_t *mf)
{
  usb_dev_handle *hdev;
  int ret = 0;
  unsigned char *buf;
  unsigned char *p, *pend;
  int valbytes = 1;
  int valendian = 0;
  int i, r, c;
  unsigned long mem_adr = mem_start;
  unsigned long mem_len = mem_length;
  unsigned long len;
  unsigned long max_block = 0x400;
  unsigned int elsize = 4;

  if (mem_len == 0)
    mem_len = elsize * mf->rows * mf->cols;

  if (max_block > 0x400)
    max_block = 0x400;

  buf = malloc(mem_len);
  if (buf == NULL)
  {
    fprintf(stderr, "upload_matrix : not enough memory for buffer\n");
    goto err_malloc;
  };

  hdev = usb_open_device(vid, pid);

  if (!hdev)
  {
    perror("upload_matrix : open failed");
    goto err_usb_open;
  };

  if (ret < 0)
  {
    printf("ERR ctrl msg\n");
    mem_len = 0;
  }

  p = buf;
  pend = buf + mem_len;
  while (p < pend)
  {
    len = pend - p;
    if (len > max_block)
      len = max_block;

    /* read data */
    i = 3;

    do
    {
      ret = usb_control_msg(hdev, USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN, USB_VENDOR_GET_SET_MEMORY | mem_type /*USB_VENDOR_TARGET_XDATA*/,
                            mem_adr & 0xffff, (mem_adr >> 16) & 0xffff, (void *)p, len, 100 + len); //USB_TIMEOUT);
//      ret = usb_bulk_read( hdev, USB_ENDPOINT_IN | 0x02, buf, len, 1000);

      if (verbose && ret < 0)
        printf("Mem read error - again\n");

      i--;
    }
    while (ret < 0 && i);

    if (ret < 0)
    {
      fprintf(stderr, "Mem read returns %d: %s\n", ret, usb_strerror());
      break;
    }
    p += len;
    mem_adr += len;
  }

  for (r = 0; r < mf->rows; r++) {
    for (c = 0; c < mf->cols; c++) {
      void *pval;
      if (!mf->transpose)
        pval = buf + (r * mf->cols + c) * elsize;
      else
        pval = buf + (c * mf->rows + r) * elsize;
      fprintf(file, "%s%13e", c?" ":"", *(float*)pval);
    }
    fprintf(file, "\n");
  }
  usb_close_device(hdev);

  free(buf);

  return 0;

 err_usb_open:
  free(buf);
 err_malloc:
  return -1;
};


/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

int si_long(char **ps, long *val, int base)
{
  char *p;
  *val = strtol(*ps, &p, base);
  if (*ps == p) return -1;
  *ps = p;
  return 1;
}

int si_ulong(char **ps, unsigned long *val, int base)
{
  char *p;
  *val = strtoul(*ps, &p, base);
  if (*ps == p) return -1;
  *ps = p;
  return 1;
}

int add_to_arr(void **pdata, int *plen, int base, char *str,
               int valbytes, int valendian)
{
  char *s = str;
  unsigned long val;
  unsigned char *p;
  int i;
  int dir = valendian? -1: 1;

  if (!valbytes)
    valbytes = 1;
  if (valbytes > 4)
    return -1;

  do{
    while (*s && strchr(", \t:;" ,*s)) s++;
    if (!*s) break;
    if (si_ulong(&s, &val, base) < 0)
    {
      return -1;
    }
    if(*pdata == NULL)
    {
      *plen = 0;
      *pdata = p = malloc(valbytes);
    }
    else
    {
      p = realloc(*pdata, *plen + valbytes);
      if(p == NULL) return -1;
      *pdata = p;
    }

    p += *plen;
    if (dir < 0)
      p += valbytes - 1;
    for(i = valbytes; i--; )
    {
      *p = val;
      val >>= 8;
      p += dir;
    }
    (*plen) += valbytes;
  } while(1);
  return 1;
}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

static void
usage(void)
{
  printf("Usage: usb_sendhex <parameters> <hex_file>\n");
  printf("  -d, --vid <num>          device vendor id (VID) [0x%04X]\n", USB_DEV_VID);
  printf("  -i, --pid <num>          product id (PID) [0x%04X]\n", USB_DEV_PID);
  printf("  -t, --type <num>         target module memory space\n");
  printf("  -s, --start <addr>       start address of transfer\n");
  printf("  -l, --length <num>       length of upload block\n");
  printf("  -r, --rows <num>         number of rows\n");
  printf("  -c, --cols <num>         number of columns\n");
  printf("  -f, --format <format>    format\n");
  printf("  -v, --verbose            verbose program\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc, char *argv[])
{
  static struct option long_opts[] =
  {
    { "vid", 1, 0, 'd' },
    { "pid", 1, 0, 'i' },
    { "type",  1, 0, 't' },
    { "rows", 1, 0, 'r' },
    { "colss", 1, 0, 'c' },
    { "start", 1, 0, 's' },
    { "length", 1, 0, 'l' },
    { "format", 1, 0, 'f' },
    { "verbose", 0, 0, 'v' },
    { "version", 0, 0, 'V' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int opt;

#ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:i:t:s:l:r:c:vVh")) != EOF)
#else
  while ((opt = getopt_long(argc, argv, "d:i:t:s:l:r:c:vVh", &long_opts[0], NULL)) != EOF)
#endif

    switch (opt)
    {
      char *p;
      case 'd':
        vid = strtol(optarg, &p, 16);

        if (!p || (p == optarg))
        {
          printf("usb_sendhex : vendor ID is not hexadecimal number\n");
          exit(1);
        }

        if (*p == ':')
        {
          char *r = p + 1;
          pid = strtol(r, &p, 16);

          if (!p || (p == r))
          {
            printf("usb_sendhex : product ID is not hexadecimal number\n");
            exit(1);
          }
        }

        break;
      case 'i':
        pid = strtol(optarg, &p, 16);
        break;
      case 't':
        mem_type = strtol(optarg, NULL, 0);
        break;
      case 's':
        mem_start = strtoul(optarg, NULL, 0);
        break;
      case 'l':
        mem_length = strtoul(optarg, NULL, 0);
        mem_length_flg = 1;
        break;
      case 'r':
        mat_format.rows = strtoul(optarg, NULL, 0);
        break;
      case 'c':
        mat_format.cols = strtoul(optarg, NULL, 0);
        break;
      case 'f':
        file_format = optarg;
        break;
      case 'v':
        verbose = 1;
        break;
      case 'V':
        fputs("USB fetchmat v.1.1\n", stdout);
        exit(0);
      case 'h':
      default:
        usage();
        exit(opt == 'h' ? 0 : 1);
    }

  {
    if (upload_matrix(stdout, &mat_format) < 0)
      exit(2);
  }

  return 0;
}
