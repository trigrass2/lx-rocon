library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.lx_rocon_pkg.all;
use work.util_pkg.all;

-- Increments step in IRC coprocessor

entity irc_proc_inc is
	generic
	(
		num_irc_g  : positive := 4
	);
	port
	(
		-- Clock
		clk_i   : in std_logic;
		reset_i : in std_logic;
		-- Output
		op_o    : out std_logic_vector(1 downto 0);
		axis_o  : out std_logic_vector((ceil_log2(num_irc_g)-1) downto 0)
	);
end irc_proc_inc;

architecture Behavioral of irc_proc_inc is

	signal num_s : std_logic_vector((axis_o'length+1) downto 0);

begin

	-- higher bits are axis
	axis_o <= num_s((num_s'length-1) downto 2);
	-- lower bits are op
	op_o <= num_s(1 downto 0);

inc:
	process
	begin

		wait until clk_i'event and clk_i = '1';

		if reset_i = '1' then
			num_s <= std_logic_vector(to_unsigned(4*num_irc_g - 1, num_s'length));
		elsif num_s = std_logic_vector(to_unsigned(4*num_irc_g - 1, num_s'length)) then
			num_s <= (others => '0');
		else
			num_s <= std_logic_vector(num_s + 1);
		end if;

	end process;

end Behavioral;

