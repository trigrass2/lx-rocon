LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;

LIBRARY std;
USE std.textio.all;

ENTITY lx_rocon_top_tb IS
END lx_rocon_top_tb;

ARCHITECTURE behavior OF lx_rocon_top_tb IS

    -- Component Declaration for the Unit Under Test (UUT)

    COMPONENT lx_rocon_top
    PORT(
         --clk_cpu : IN  std_logic;
				 clk_50m : IN  std_logic;
         cs0_xc : IN  std_logic;
         rd : IN  std_logic;
         bls : IN  std_logic_vector(3 downto 0);
         address : IN  std_logic_vector(15 downto 0);
         data : INOUT  std_logic_vector(31 downto 0);
				 irc0_a : IN  std_logic;
         irc0_b : IN  std_logic;
         irc0_index : IN  std_logic;
         irc0_mark : IN  std_logic;
         irc1_a : IN  std_logic;
         irc1_b : IN  std_logic;
         irc1_index : IN  std_logic;
         irc1_mark : IN  std_logic;
         irc2_a : IN  std_logic;
         irc2_b : IN  std_logic;
         irc2_index : IN  std_logic;
         irc2_mark : IN  std_logic;
         irc3_a : IN  std_logic;
         irc3_b : IN  std_logic;
         irc3_index : IN  std_logic;
         irc3_mark : IN  std_logic;
         irc4_a : IN  std_logic;
         irc4_b : IN  std_logic;
         irc4_index : IN  std_logic;
         irc4_mark : IN  std_logic;
				 irc5_a : IN  std_logic;
         irc5_b : IN  std_logic;
         irc5_index : IN  std_logic;
         irc5_mark : IN  std_logic;
				 irc6_a : IN  std_logic;
         irc6_b : IN  std_logic;
         irc6_index : IN  std_logic;
         irc6_mark : IN  std_logic;
				 irc7_a : IN  std_logic;
         irc7_b : IN  std_logic;
         irc7_index : IN  std_logic;
         irc7_mark : IN  std_logic;
         init : IN  std_logic;
         s1_clk_in : IN  std_logic;
         s1_miso : IN  std_logic;
				 s1_sync_in : IN  std_logic;
         s1_clk_out : OUT  std_logic;
				 s1_mosi : OUT  std_logic;
         s1_sync_out : OUT  std_logic
        );
    END COMPONENT;


   --Inputs
   --signal clk_cpu : std_logic := '0';
	 signal clk_50m : std_logic := '0';
   signal cs0_xc : std_logic := '1';
   signal rd : std_logic := '1';
   signal bls : std_logic_vector(3 downto 0) := (others => '1');
   signal address : std_logic_vector(15 downto 0) := (others => '0');
	 signal irc0_a : std_logic := '0';
   signal irc0_b : std_logic := '0';
   signal irc0_index : std_logic := '0';
   signal irc0_mark : std_logic := '0';
   signal irc1_a : std_logic := '0';
   signal irc1_b : std_logic := '0';
   signal irc1_index : std_logic := '0';
   signal irc1_mark : std_logic := '0';
   signal irc2_a : std_logic := '0';
   signal irc2_b : std_logic := '0';
   signal irc2_index : std_logic := '0';
   signal irc2_mark : std_logic := '0';
   signal irc3_a : std_logic := '0';
   signal irc3_b : std_logic := '0';
   signal irc3_index : std_logic := '0';
   signal irc3_mark : std_logic := '0';
   signal irc4_a : std_logic := '0';
   signal irc4_b : std_logic := '0';
   signal irc4_index : std_logic := '0';
   signal irc4_mark : std_logic := '0';
	 signal irc5_a : std_logic := '0';
   signal irc5_b : std_logic := '0';
   signal irc5_index : std_logic := '0';
   signal irc5_mark : std_logic := '0';
	 signal irc6_a : std_logic := '0';
   signal irc6_b : std_logic := '0';
   signal irc6_index : std_logic := '0';
   signal irc6_mark : std_logic := '0';
	 signal irc7_a : std_logic := '0';
   signal irc7_b : std_logic := '0';
   signal irc7_index : std_logic := '0';
   signal irc7_mark : std_logic := '0';
   signal init : std_logic := '1';
	 signal s1_clk_in : std_logic := '1';
	 signal s1_miso : std_logic := '1';
	 signal s1_sync_in : std_logic := '1';

	 --Outputs
	 signal s1_clk_out : std_logic := '1';
	 signal s1_mosi : std_logic := '1';
	 signal s1_sync_out : std_logic := '1';

	 --BiDirs
   signal data : std_logic_vector(31 downto 0);

   -- Clock period definitions
	 --constant clk_period_cpu : time := 13.8 ns;
   constant clk_period_50m : time := 20 ns;

BEGIN

	-- Instantiate the Unit Under Test (UUT)
   uut: lx_rocon_top PORT MAP (
          --clk_cpu => clk_cpu,
					clk_50m => clk_50m,
          cs0_xc => cs0_xc,
          rd => rd,
          bls => bls,
          address => address,
          data => data,
					irc0_a => irc0_a,
          irc0_b => irc0_b,
          irc0_index => irc0_index,
          irc0_mark => irc0_mark,
          irc1_a => irc1_a,
          irc1_b => irc1_b,
          irc1_index => irc1_index,
          irc1_mark => irc1_mark,
          irc2_a => irc2_a,
          irc2_b => irc2_b,
          irc2_index => irc2_index,
          irc2_mark => irc2_mark,
          irc3_a => irc3_a,
          irc3_b => irc3_b,
          irc3_index => irc3_index,
          irc3_mark => irc3_mark,
          irc4_a => irc4_a,
          irc4_b => irc4_b,
          irc4_index => irc4_index,
          irc4_mark => irc4_mark,
					irc5_a => irc5_a,
          irc5_b => irc5_b,
          irc5_index => irc5_index,
          irc5_mark => irc5_mark,
					irc6_a => irc6_a,
          irc6_b => irc6_b,
          irc6_index => irc6_index,
          irc6_mark => irc6_mark,
					irc7_a => irc7_a,
          irc7_b => irc7_b,
          irc7_index => irc7_index,
          irc7_mark => irc7_mark,
          init => init,
					s1_clk_in => s1_clk_in,
					s1_miso => s1_miso,
					s1_sync_in => s1_sync_in,
					s1_clk_out => s1_clk_out,
					s1_mosi => s1_mosi,
					s1_sync_out => s1_sync_out
        );

   -- Clock process definitions
--   clk_cpu_process :process
--   begin
--		clk_cpu <= '1';
--		wait for clk_period_cpu/2;
--		clk_cpu <= '0';
--		wait for clk_period_cpu/2;
--   end process;

	 clk_50m_process :process
   begin
		clk_50m <= '1';
		wait for clk_period_50m/2;
		clk_50m <= '0';
		wait for clk_period_50m/2;
   end process;


   -- Stimulus process
   stim_proc: process
   begin
      -- External ModelSim script

      wait;
   end process;

	setup_imem_process : process
		file imem_file : text open READ_MODE is "imem.bits";
		variable my_line : LINE;
		variable bits_line : LINE;
		variable mem_location : bit_vector(31 downto 0);
		variable imem_fill_addr : natural range 0 to 2**8-1 := 0;
	begin

		-- Assert ROCON system reset for 3 clock cycles
		wait until clk_50m'event and clk_50m = '1';
		init <= '0';
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '1';
		init <= '1';

		-- Fill Tumbl instruction memory
		fill_loop: while not endfile(imem_file) loop
			wait until clk_50m'event and clk_50m = '1';
			cs0_xc <= '1';
			rd <= '1';
			bls <= "1111";
			wait until clk_50m'event and clk_50m = '1';
			address <= std_logic_vector(to_unsigned(imem_fill_addr, 16));
			readline(imem_file, bits_line);
			read(bits_line, mem_location);
			data <= to_stdLogicVector(mem_location);
			bls <= "0000";
			cs0_xc <= '0';
			imem_fill_addr := imem_fill_addr + 1;
			wait until clk_50m'event and clk_50m = '1';
			wait until clk_50m'event and clk_50m = '1';
			cs0_xc <= '1';
			rd <= '1';
			bls <= "1111";
		end loop fill_loop;

		-- Negate Tumbl reset state in Tumbl control register
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '1';
		address <= x"0C00";
		data <= x"00000000";
		bls <= "0000";
		cs0_xc <= '0';
		wait until clk_50m'event and clk_50m = '1';
		wait until clk_50m'event and clk_50m = '1';
		cs0_xc <= '1';
		rd <= '1';
		bls <= "1111";

		-- Simulate external master accesses to Tumbl shared xmem bus
		xmem_loop: loop
			wait until clk_50m'event and clk_50m = '1';
			wait until clk_50m'event and clk_50m = '1';
			address <= x"8808";
			rd <= '0';
			cs0_xc <= '0';
			wait until clk_50m'event and clk_50m = '1';
			wait until clk_50m'event and clk_50m = '1';
			wait until clk_50m'event and clk_50m = '1';
			cs0_xc <= '1';
			rd <= '1';
			bls <= "1111";
		end loop xmem_loop;

		wait;

	end process;

END;
