library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.lx_fncapprox_pkg.all;

-- IRC bus interconnect
entity lx_fncapprox is
	port
	(
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Data bus
		address_i    : in std_logic_vector(4 downto 0);
		ce_i         : in std_logic;
		data_i       : in std_logic_vector(31 downto 0);
		data_o       : out std_logic_vector(31 downto 0);
		--
		bls_i        : in std_logic_vector(3 downto 0)
	);
end lx_fncapprox;

-- reciproc ...  a + (b + c * x) * x
-- sin      ...  a + (b + c * x) * x


architecture Behavioral of lx_fncapprox is

	-- Types

	type state_fncapprox_t is (ST_IDLE, ST_RECI0, ST_RECI1,
	                 ST_SIN0, ST_SIN1, ST_COS0, ST_COS1);

	-- Signals

	constant approx_top_bits    : natural := 9;
	constant reci_tab_bits      : natural := 8;
	constant sin_tab_bits       : natural := 7;
	constant approx_frac_bits   : natural := 18;
	constant approx_tab_b_shift : natural := 8;
	constant approx_tab_c_shift : natural := 25;

	constant approx_lo_used_bit : natural := 31 - approx_top_bits - approx_frac_bits + 1;

	signal ce_r                 : std_logic;
	signal address_r            : std_logic_vector(4 downto 0);

	signal argument_s           : std_logic_vector(31 downto 0);
	signal argument_r           : std_logic_vector(31 downto 0);

	signal dsp48_p_s            : std_logic_vector(47 downto 0);
	signal dsp48_a_s            : std_logic_vector(17 downto 0);
	signal dsp48_b_s            : std_logic_vector(17 downto 0);
	signal dsp48_c_s            : std_logic_vector(47 downto 0);
	signal dsp48_ce_s           : std_logic;

	signal state_tab_s          : state_fncapprox_t;
	signal state_tab_r          : state_fncapprox_t;
	signal state_dsp48_r        : state_fncapprox_t;
	signal state_regw_r         : state_fncapprox_t;

	signal reci_tab_idx_s       : std_logic_vector(reci_tab_bits-1 downto 0);
	signal approx_frac_s        : std_logic_vector(approx_frac_bits-1 downto 0);
	signal approx_frac_r        : std_logic_vector(approx_frac_bits-1 downto 0);
	signal reci_tab_a_data_s    : std_logic_vector(35 downto 0);
	signal reci_tab_a_data_r    : std_logic_vector(35 downto 0);
	signal reci_tab_bc_data_s   : std_logic_vector(35 downto 0);
	signal reci_tab_bc_data_r   : std_logic_vector(35 downto 0);

	signal sin_tab_idx_s        : std_logic_vector(sin_tab_bits-1 downto 0);
	signal sin_tab_a_data_s     : std_logic_vector(35 downto 0);
	signal sin_tab_bc_data_s    : std_logic_vector(35 downto 0);

	signal approx_tab_b_shifted_r : std_logic_vector(47 downto 0);
	signal reci_tab_a_shifted_r : std_logic_vector(47 downto 0);
	signal frac_c_mult_s        : std_logic_vector(47 downto 0);
	signal frac2_bc_mult_s      : std_logic_vector(47 downto 0);

	signal reci_result_s        : std_logic_vector(31 downto 0);
	signal reci_result_r        : std_logic_vector(31 downto 0);
	signal sin_result_s         : std_logic_vector(31 downto 0);
	signal sin_result_r         : std_logic_vector(31 downto 0);
	signal cos_result_s         : std_logic_vector(31 downto 0);
	signal cos_result_r         : std_logic_vector(31 downto 0);

	signal reci_tab_a_stb_s     : std_logic;
	signal reci_tab_bc_stb_s    : std_logic;
	signal sin_tab_a_stb_s      : std_logic;
	signal sin_tab_bc_stb_s     : std_logic;

	signal sin_cos_force_one_s  : std_logic;
	signal sin_cos_force_one_r  : std_logic;
	signal sin_cos_force_one_r2 : std_logic;
	signal sin_cos_negate_out_s : std_logic;
	signal sin_cos_negate_out_r : std_logic;
	signal sin_cos_negate_out_r2: std_logic;
begin

approx_dsp48: lx_fncapprox_dsp48
	port map (
		P         => dsp48_p_s,
		A         => dsp48_a_s,
		B         => dsp48_b_s,
		C         => dsp48_c_s,
		CLK       => clk_i,
		CE        => dsp48_ce_s
	);

reci_tab_a : rom_table
	generic map (
		data_width => 36,
		addr_width => reci_tab_bits,
		init_file  => "reci_tab_a.lut"
	)
	port map
	(
		clk_i  => clk_i,
		stb_i  => reci_tab_a_stb_s,
		addr_i => reci_tab_idx_s,
		data_o => reci_tab_a_data_s,
		ack_o => open
	);

reci_tab_bc : rom_table
	generic map (
		data_width => 36,
		addr_width => reci_tab_bits,
		init_file  => "reci_tab_bc.lut"
	)
	port map
	(
		clk_i  => clk_i,
		stb_i  => reci_tab_bc_stb_s,
		addr_i => reci_tab_idx_s,
		data_o => reci_tab_bc_data_s,
		ack_o => open
	);

sin_tab_a : rom_table
	generic map (
		data_width => 36,
		addr_width => sin_tab_bits,
		init_file  => "sin_tab_a.lut"
	)
	port map
	(
		clk_i  => clk_i,
		stb_i  => sin_tab_a_stb_s,
		addr_i => sin_tab_idx_s,
		data_o => sin_tab_a_data_s,
		ack_o => open
	);

sin_tab_bc : rom_table
	generic map (
		data_width => 36,
		addr_width => sin_tab_bits,
		init_file  => "sin_tab_bc.lut"
	)
	port map
	(
		clk_i  => clk_i,
		stb_i  => sin_tab_bc_stb_s,
		addr_i => sin_tab_idx_s,
		data_o => sin_tab_bc_data_s,
		ack_o => open
	);

wire_in_and_next_state:
	process(ce_i, ce_r, bls_i, address_i, data_i, state_tab_r, argument_r)
	begin
		-- Incoming bus request
		if (ce_i = '1') and (bls_i(0) = '1') then
			argument_s <= data_i;
			if address_i(4 downto 0) = "00001" then
				state_tab_s <= ST_RECI0;
			elsif address_i(4 downto 0) = "00010" then
				state_tab_s <= ST_SIN0;
			elsif address_i(4 downto 0) = "00011" then
				state_tab_s <= ST_COS0;
			else
				state_tab_s <= ST_IDLE;
			end if;
		else
			argument_s <= argument_r;
			case state_tab_r is
				when ST_IDLE =>
					state_tab_s <= ST_IDLE;
				when ST_RECI0 =>
					state_tab_s <= ST_RECI1;
				when ST_RECI1 =>
					state_tab_s <= ST_IDLE;
				when ST_SIN0 =>
					state_tab_s <= ST_SIN1;
				when ST_SIN1 =>
					state_tab_s <= ST_COS0;
				when ST_COS0 =>
					state_tab_s <= ST_COS1;
				when ST_COS1 =>
					state_tab_s <= ST_IDLE;
			end case;
		end if;
	end process;

function_tables_access:
	process(state_tab_r, argument_r)
		variable negate_arg_v : std_logic;
		variable arg_frac_is_zeros_v : std_logic;
		variable approx_frac_v : std_logic_vector(approx_frac_bits-1 downto 0);
	begin
		-- init values
		reci_tab_idx_s <= (others => '-');
		sin_tab_idx_s <= (others => '-');
		approx_frac_s <= (others => '-');
		sin_cos_force_one_s <= '-';
		sin_cos_negate_out_s <= '-';

		negate_arg_v := '-';

		reci_tab_a_stb_s   <= '0';
		reci_tab_bc_stb_s  <= '0';
		sin_tab_a_stb_s    <= '0';
		sin_tab_bc_stb_s   <= '0';

		if argument_r(29 downto approx_lo_used_bit) = (28 - approx_lo_used_bit downto 0 => '0') then
			arg_frac_is_zeros_v := '1';
		else
			arg_frac_is_zeros_v := '0';
		end if;

		case state_tab_r is
			when ST_IDLE =>
				null;

			when ST_RECI0 =>
				reci_tab_idx_s <= argument_r(30 downto
					30 - reci_tab_bits + 1);
				reci_tab_bc_stb_s <= '1';
				negate_arg_v := '0';

			when ST_RECI1 =>
				reci_tab_idx_s <= argument_r(30 downto
					30 - reci_tab_bits + 1);
				reci_tab_a_stb_s <= '1';
				negate_arg_v := '0';

			when ST_SIN0 =>
				sin_tab_bc_stb_s <= '1';
				sin_tab_a_stb_s <= '1';
				negate_arg_v := argument_r(30);

			when ST_SIN1 =>
				if (arg_frac_is_zeros_v = '1') and (argument_r(30) = '1') then
					sin_cos_force_one_s <= '1';
				else
					sin_cos_force_one_s <= '0';
				end if;

				if argument_r(31) = '1' then
					sin_cos_negate_out_s <= '1';
				else
					sin_cos_negate_out_s <= '0';
				end if;

				sin_tab_bc_stb_s <= '1';
				negate_arg_v := argument_r(30);

			when ST_COS0 =>
				sin_tab_bc_stb_s <= '1';
				sin_tab_a_stb_s <= '1';
				negate_arg_v := not argument_r(30);

			when ST_COS1 =>
				if (arg_frac_is_zeros_v = '1') and (argument_r(30) = '0') then
					sin_cos_force_one_s <= '1';
				else
					sin_cos_force_one_s <= '0';
				end if;

				if argument_r(31) /= argument_r(30) then
					sin_cos_negate_out_s <= '1';
				else
					sin_cos_negate_out_s <= '0';
				end if;

				sin_tab_bc_stb_s <= '1';
				negate_arg_v := not argument_r(30);
		end case;

		approx_frac_v := argument_r(31 - approx_top_bits downto
				31 - approx_top_bits - approx_frac_bits + 1);
		approx_frac_v(approx_frac_bits - 1) := not approx_frac_v(approx_frac_bits - 1);

		if negate_arg_v = '0' then
			sin_tab_idx_s <= argument_r(29 downto
				29 - sin_tab_bits + 1);
			approx_frac_s <= approx_frac_v;
		else
			sin_tab_idx_s <= not argument_r(29 downto
				29 - sin_tab_bits + 1);
			approx_frac_s <= not approx_frac_v;
		end if;
	end process;

dsp48_computation:
	process(state_dsp48_r, approx_frac_r, dsp48_p_s,
	        reci_tab_a_data_s, reci_tab_bc_data_s,
	        sin_tab_a_data_s, sin_tab_bc_data_s)
	begin
		dsp48_a_s <= (others => '-');
		dsp48_b_s <= (others => '-');
		dsp48_c_s <= (others => '-');
		dsp48_ce_s <= '0';

		case state_dsp48_r is
			when ST_IDLE =>
				null;

			when ST_RECI0 =>
				-- yl = reci_tab_a[ti];
				-- yl -= ((reci_tab_b[ti] - ((reci_tab_c[ti] * xd) >> approx_tab_c_shift)) * xd) >> approx_tab_b_shift;

				dsp48_c_s(approx_tab_c_shift - 1 downto 0) <= (others => '0');
				dsp48_c_s(approx_tab_c_shift + 17 downto approx_tab_c_shift) <= reci_tab_bc_data_s(35 downto 18);
				dsp48_c_s(47 downto approx_tab_c_shift + 18) <= (others => reci_tab_bc_data_s(35));

				dsp48_a_s <= reci_tab_bc_data_s(17 downto 0);
				dsp48_b_s <= approx_frac_r;
				dsp48_ce_s <= '1';

			when ST_RECI1 =>
				dsp48_c_s(approx_tab_b_shift - 1 downto 0) <= (others => '0');
				dsp48_c_s(approx_tab_b_shift + 35 downto approx_tab_b_shift) <= reci_tab_a_data_s;
				dsp48_c_s(47 downto approx_tab_b_shift + 36) <= (others => '0');

				dsp48_a_s <= dsp48_p_s(approx_tab_c_shift + 17 downto approx_tab_c_shift);
				dsp48_b_s <= approx_frac_r;
				dsp48_ce_s <= '1';

			when ST_SIN0 | ST_COS0 =>
				dsp48_c_s(approx_tab_c_shift - 1 downto 0) <= (others => '0');
				dsp48_c_s(approx_tab_c_shift + 17 downto approx_tab_c_shift) <= sin_tab_bc_data_s(35 downto 18);
				dsp48_c_s(47 downto approx_tab_c_shift + 18) <= (others => sin_tab_bc_data_s(35));

				dsp48_a_s <= sin_tab_bc_data_s(17 downto 0);
				dsp48_b_s <= approx_frac_r;
				dsp48_ce_s <= '1';

			when ST_SIN1 | ST_COS1 =>
				dsp48_c_s(approx_tab_b_shift - 1 downto 0) <= (others => '0');
				dsp48_c_s(approx_tab_b_shift + 35 downto approx_tab_b_shift) <= sin_tab_a_data_s;
				dsp48_c_s(47 downto approx_tab_b_shift + 36) <= (others => '0');

				dsp48_a_s <= dsp48_p_s(approx_tab_c_shift + 17 downto approx_tab_c_shift);
				dsp48_b_s <= approx_frac_r;
				dsp48_ce_s <= '1';

		end case;
	end process;

result_registers_write:
	process(state_regw_r, dsp48_p_s, sin_cos_negate_out_r2, sin_cos_force_one_r2,
		reci_result_r, sin_result_r, cos_result_r)
		variable sin_cos_res_v  : std_logic_vector(31 downto 0);
		variable sin_cos_res_short_v  : std_logic_vector(17 downto 0);
	begin
		reci_result_s <= reci_result_r;
		sin_result_s <= sin_result_r;
		cos_result_s <= cos_result_r;

		if sin_cos_force_one_r2 = '1' then
			sin_cos_res_v := (30 => '1', others => '0');
		else
			sin_cos_res_v := dsp48_p_s(approx_tab_b_shift + 35 -1 downto approx_tab_b_shift + 4 - 1);
		end if;

		-- Precise 2'nd complement
		-- if sin_cos_negate_out_r2 = '1' then
		-- 	sin_cos_res_v := std_logic_vector(-signed(sin_cos_res_v));
                -- end if;

		-- Fast imprecise first complement
		if sin_cos_negate_out_r2 = '1' then
			sin_cos_res_v := not sin_cos_res_v;
                end if;

		sin_cos_res_short_v := std_logic_vector(unsigned(sin_cos_res_v(31 downto 14)) +
		                                        unsigned(sin_cos_res_v(13 downto 13)));

		case state_regw_r is
			when ST_IDLE =>
				null;

			when ST_RECI0 =>
				null;

			when ST_RECI1 =>
				reci_result_s <= dsp48_p_s(approx_tab_b_shift + 35 downto approx_tab_b_shift + 4);

			when ST_SIN0 =>
				null;

			when ST_SIN1 =>
				sin_result_s(17 downto 0) <= sin_cos_res_short_v;
				sin_result_s(31 downto 18) <= (others => sin_cos_res_short_v(17));

			when ST_COS0 =>
				null;

			when ST_COS1 =>
				cos_result_s(17 downto 0) <= sin_cos_res_short_v;
				cos_result_s(31 downto 18) <= (others => sin_cos_res_short_v(17));

		end case;
	end process;

wire_out:
	process(ce_r, reci_result_r, sin_result_r, cos_result_r, address_r, argument_r)
	begin
		data_o <= (others => '-');

		if ce_r = '1' then
			if address_r(4 downto 0) = "00000" then
				data_o <= x"FA00000E";
			elsif address_r(4 downto 0) = "00001" then
				data_o <= reci_result_r;
			elsif address_r(4 downto 0) = "00010" then
				data_o <= sin_result_r;
			elsif address_r(4 downto 0) = "00011" then
				data_o <= cos_result_r;
			else
				data_o <= (others => '0');
			end if;
		end if;
	end process;

update:
	process
	begin
		wait until clk_i'event and clk_i= '1';
		ce_r      <= ce_i;
		address_r <= address_i;

		approx_frac_r <= approx_frac_s;

		state_tab_r <= state_tab_s;
		state_dsp48_r <= state_tab_r;
		state_regw_r <= state_dsp48_r;

		argument_r <= argument_s;
		sin_cos_force_one_r <= sin_cos_force_one_s;
		sin_cos_force_one_r2 <= sin_cos_force_one_r;
		sin_cos_negate_out_r <= sin_cos_negate_out_s;
		sin_cos_negate_out_r2 <= sin_cos_negate_out_r;

		reci_result_r <= reci_result_s;
		sin_result_r <= sin_result_s;
		cos_result_r <= cos_result_s;
	end process;

end Behavioral;

