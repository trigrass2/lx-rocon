library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.lx_rocon_pkg.all;

-- IRC reader module

entity irc_reader is
	port
	(
		-- Inputs
    clk_i                : in std_logic;
    reset_i              : in std_logic;
    irc_i                : in IRC_INPUT_Type;
		-- State
		reset_index_event_i  : in std_logic;
		reset_index_event2_i : in std_logic;
		reset_ab_error_i     : in std_logic;
		-- Outputs
    irc_o                : out IRC_OUTPUT_Type
  );
end irc_reader;

architecture rtl of irc_reader is
	begin

dff_mark: dff3
	port map
	(
		clk_i   => clk_i,
		d_i     => irc_i.mark,
		q_o     => irc_o.state.mark
	);

qctr: qcounter
	port map
	(
		clk_i                => clk_i,
		reset_i              => reset_i,
		a0_i                 => irc_i.a,
		b0_i                 => irc_i.b,
		index0_i             => irc_i.index,
		reset_index_event_i  => reset_index_event_i,
		reset_index_event2_i => reset_index_event2_i,
		reset_ab_error_i     => reset_ab_error_i,
		qcount_o             => irc_o.count.qcount,
		qcount_index_o       => irc_o.count.index,
		ab_error_o           => irc_o.state.ab_error,
		ab_event_o           => open,
		index_o              => irc_o.state.index,
		index_event_o        => irc_o.count.index_event,
		index_event2_o       => irc_o.state.index_event,
		a_rise_o             => open,
		a_fall_o             => open,
		b_rise_o             => open,
		b_fall_o             => open
	);

end rtl;
