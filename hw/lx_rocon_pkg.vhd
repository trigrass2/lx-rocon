library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.mbl_pkg.all;
use work.util_pkg.all;

-- Entities within lx_rocon

package lx_rocon_pkg is

	-- Types
	type IRC_INPUT_Type is record
		a, b  : std_logic;
		index : std_logic;
		mark  : std_logic;
	end record;
	--
	type IRC_COUNT_OUTPUT_Type is record
		qcount      : std_logic_vector(7 downto 0);
		index       : std_logic_vector(7 downto 0);
		index_event : std_logic;
	end record;
	--
	type IRC_STATE_OUTPUT_Type is record
		ab_error     : std_logic;
		index        : std_logic;
		index_event  : std_logic;
		mark         : std_logic;
	end record;
	--
	type IRC_OUTPUT_Type is record
		count       : IRC_COUNT_OUTPUT_Type;
		state       : IRC_STATE_OUTPUT_Type;
	end record;

	-- Arrays
	type IRC_INPUT_Array_Type         is array (natural range <>) of IRC_INPUT_Type;
	type IRC_OUTPUT_Array_Type        is array (natural range <>) of IRC_OUTPUT_Type;
	type IRC_COUNT_OUTPUT_Array_Type  is array (natural range <>) of IRC_COUNT_OUTPUT_Type;
	type IRC_STATE_OUTPUT_Array_Type  is array (natural range <>) of IRC_STATE_OUTPUT_Type;

	-- IRC coprocessor MAIN
	component irc_proc_main
	generic
	(
		num_irc_g  : positive := 4
	);
	port
	(
		-- Basic input
		clk_i             : in std_logic;
		reset_i           : in std_logic;
		-- Signals from IRC
		irc_i             : in IRC_COUNT_OUTPUT_Array_Type((num_irc_g-1) downto 0);
		-- Index resetting
		irc_index_reset_o : out std_logic_vector((num_irc_g-1) downto 0);
		-- BRAM access
		mem_clk_i         : in std_logic;
    mem_en_i          : in std_logic;
    mem_we_i          : in std_logic_vector(3 downto 0);
    mem_addr_i        : in std_logic_vector(ceil_log2(num_irc_g) downto 0);
    mem_data_i        : in std_logic_vector(31 downto 0);
    mem_data_o        : out std_logic_vector(31 downto 0)
	);
	end component;

	-- IRC coprocessor INC
	component irc_proc_inc
	generic
	(
		num_irc_g  : positive := 4
	);
	port
	(
		-- Clock
		clk_i   : in std_logic;
		reset_i : in std_logic;
		-- Output
		op_o    : out std_logic_vector(1 downto 0);
		axis_o  : out std_logic_vector((ceil_log2(num_irc_g)-1) downto 0)
	);
	end component;

	-- IRC reader
	component irc_reader
	port
	(
		-- Inputs
    clk_i                : in std_logic;
    reset_i              : in std_logic;
    irc_i                : in IRC_INPUT_Type;
		-- State
		reset_index_event_i  : in std_logic;
		reset_index_event2_i : in std_logic;
		reset_ab_error_i     : in std_logic;
		-- Outputs
    irc_o                : out IRC_OUTPUT_Type
  );
	end component;

	-- Quadcount
	component qcounter
	port
	(
		-- Inputs
    clk_i                : in std_logic;
    reset_i              : in std_logic;
    a0_i, b0_i           : in std_logic;
		index0_i             : in std_logic;
		-- State
		reset_index_event_i  : in std_logic;
		reset_index_event2_i : in std_logic;
		reset_ab_error_i     : in std_logic;
		-- Outputs
    qcount_o             : out std_logic_vector(7 downto 0);
		qcount_index_o       : out std_logic_vector(7 downto 0);
		index_o              : out std_logic;
		index_event_o        : out std_logic;
		index_event2_o       : out std_logic;
    a_rise_o, a_fall_o   : out std_logic;
		b_rise_o, b_fall_o   : out std_logic;
		ab_event_o           : out std_logic;
    ab_error_o           : out std_logic
  );
	end component;

	-- D sampler (filtered, 2 cycles)
	component dff2
	port
	(
    clk_i   : in std_logic;
    d_i     : in std_logic;
    q_o     : out std_logic
  );
  end component;

	-- D sampler (filtered, 3 cycles)
	component dff3
	port
	(
    clk_i   : in std_logic;
    d_i     : in std_logic;
    q_o     : out std_logic
  );
  end component;

	-- CRC8
	component crc
  port
	(
		clk_i   : in std_logic;
    reset_i : in std_logic;
    input_i : in std_logic;
    crc_o   : out std_logic_vector(7 downto 0)
  );
	end component;

	-- Counter - divider
	component cnt_div
	generic (
		cnt_width_g : natural := 8
	);
	port
	(
		clk_i     : in std_logic;
		en_i      : in std_logic;
		reset_i   : in std_logic;
		ratio_i   : in std_logic_vector(cnt_width_g-1 downto 0);
		q_out_o   : out std_logic
	);
	end component;

	-- LX Master transmitter
	component lxmaster_transmitter
	generic (
		cycle_cnt_width_g : natural := 12
	);
	port
	(
		clk_i             : in std_logic;
		reset_i           : in std_logic;
		-- Transmision
		clock_o           : out std_logic;
		mosi_o            : out std_logic;
		sync_o            : out std_logic;
		-- Register
		register_i        : in std_logic;
		register_o        : out std_logic_vector(1 downto 0);
		register_we_i     : in std_logic;
		-- Cycle period
		cycle_reg_i       : in std_logic_vector(cycle_cnt_width_g-1 downto 0);
		cycle_reg_o       : out std_logic_vector(cycle_cnt_width_g-1 downto 0);
		cycle_reg_we_i    : in std_logic;
		-- Watchdog
		wdog_i            : in std_logic;
		wdog_we_i         : in std_logic;
		-- BRAM access
		mem_clk_i         : in std_logic;
    mem_en_i          : in std_logic;
    mem_we_i          : in std_logic_vector(1 downto 0);
    mem_addr_i        : in std_logic_vector(8 downto 0);
    mem_data_i        : in std_logic_vector(15 downto 0);
    mem_data_o        : out std_logic_vector(15 downto 0)
	);
	end component;

	-- LX Master receiver
	component lxmaster_receiver
	port
	(
		clk_i             : in std_logic;
		reset_i           : in std_logic;
		-- Transmision
		clock_i           : in std_logic;
		miso_i            : in std_logic;
		sync_i            : in std_logic;
		-- Receive done pulse
		rx_done_o         : out std_logic;
		rx_crc_error_o    : out std_logic;
		-- Register
		register_i        : in std_logic;
		register_o        : out std_logic_vector(1 downto 0);
		register_we_i     : in std_logic;
		-- BRAM access
		mem_clk_i         : in std_logic;
		mem_en_i          : in std_logic;
		mem_we_i          : in std_logic_vector(1 downto 0);
		mem_addr_i        : in std_logic_vector(8 downto 0);
		mem_data_i        : in std_logic_vector(15 downto 0);
		mem_data_o        : out std_logic_vector(15 downto 0)
	);
	end component;

	-- LX math functions approximation

	component lx_fncapprox
	port
	(
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Data bus
		address_i    : in std_logic_vector(4 downto 0);
		ce_i         : in std_logic;
		data_i       : in std_logic_vector(31 downto 0);
		data_o       : out std_logic_vector(31 downto 0);
		--
		bls_i        : in std_logic_vector(3 downto 0)
	);
	end component;

	-- Clock Cross Domain Synchronization Elastic Buffer/FIFO
	component lx_crosdom_ser_fifo
	generic
	(
		fifo_len_g   : positive := 8;
		sync_adj_g   : integer := 0
	);
	port
	(
		-- Asynchronous clock domain interface
		acd_clock_i  : in std_logic;
		acd_miso_i   : in std_logic;
		acd_sync_i   : in std_logic;
		-- Clock
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Output synchronous with clk_i
		miso_o       : out std_logic;
		sync_o       : out std_logic;
		data_ready_o : out std_logic
	);
	end component;

	--------------------------------------------------------------------------------
	-- TUMBL
	--------------------------------------------------------------------------------

	component lx_rocon_tumbl
	generic
	(
		IMEM_ABITS_g         : positive := 11;
		DMEM_ABITS_g         : positive := 12;
		--
		USE_HW_MUL_g         : boolean := true;
		USE_BARREL_g         : boolean := true;
		COMPATIBILITY_MODE_g : boolean := false
	);
	port
	(
		clk_i        :  in std_logic;
		rst_i        :  in std_logic;
	  halt_i       :  in std_logic;
		int_i        :  in std_logic;
		trace_i      :  in std_logic;
		trace_kick_i :  in std_logic;
		-- Program counter
		pc_o         : out std_logic_vector(31 downto 0);
		-- Internal halt (remove with trace kick)
		halted_o     : out std_logic;
		halt_code_o  : out std_logic_vector(4 downto 0);
		-- Internal memory (instruction)
		imem_clk_i   : in std_logic;
    imem_en_i    : in std_logic;
    imem_we_i    : in std_logic_vector(3 downto 0);
    imem_addr_i  : in std_logic_vector(8 downto 0);
    imem_data_i  : in std_logic_vector(31 downto 0);
    imem_data_o  : out std_logic_vector(31 downto 0);
		-- Internal memory (data)
		dmem_clk_i   : in std_logic;
    dmem_en_i    : in std_logic;
    dmem_we_i    : in std_logic_vector(3 downto 0);
    dmem_addr_i  : in std_logic_vector(9 downto 0);
    dmem_data_i  : in std_logic_vector(31 downto 0);
    dmem_data_o  : out std_logic_vector(31 downto 0);
		-- External memory bus
		xmemb_sel_o  : out std_logic;
		xmemb_i      : in DMEMB2CORE_Type;
		xmemb_o      : out CORE2DMEMB_Type
	);
	end component;

	component lx_rocon_imem
	port
	(
		-- Memory wiring for Tumbl
		clk_i  : in std_logic;
		cs_i   : in std_logic;
		adr_i  : in std_logic_vector(10 downto 2);
		dat_o  : out std_logic_vector(31 downto 0);
		-- Memory wiring for Master CPU
		clk_m  : in std_logic;
    en_m   : in std_logic;
    we_m   : in std_logic_vector(3 downto 0);
    addr_m : in std_logic_vector(8 downto 0);
    din_m  : in std_logic_vector(31 downto 0);
    dout_m : out std_logic_vector(31 downto 0)
	);
	end component;

	component lx_rocon_dmem
	port
	(
    -- Memory wiring for Tumbl
		clk_i  : in std_logic;
		ce_i   : in std_logic;
		adr_i  : in std_logic_vector(11 downto 2);
		bls_i  : in std_logic_vector(3 downto 0);
		dat_i  : in std_logic_vector(31 downto 0);
		dat_o  : out std_logic_vector(31 downto 0);
		-- Memory wiring for Master CPU
		clk_m  : in std_logic;
    en_m   : in std_logic;
    we_m   : in std_logic_vector(3 downto 0);
    addr_m : in std_logic_vector(9 downto 0);
    din_m  : in std_logic_vector(31 downto 0);
    dout_m : out std_logic_vector(31 downto 0)
	);
	end component;

	component lx_rocon_gprf_abd
	port
	(
		clk_i        :  in std_logic;
		rst_i        :  in std_logic;
		clken_i      :  in std_logic;
		gprf_finish_wrb_mem_i :  in std_logic;
		--
		ID2GPRF_i    :  in ID2GPRF_Type;
		MEM_WRB_i    :  in WRB_Type;
		GPRF2EX_o    :  out GPRF2EX_Type
	);
	end component;

	--------------------------------------------------------------------------------
	-- MEMORY BUS
	--------------------------------------------------------------------------------

	-- Measurement register
	component measurement_register
	generic
	(
		id_g   : std_logic_vector(31 downto 0) := (others => '0')
	);
	port
	(
		-- Clock
		clk_i    : in std_logic;
		-- Reset
		reset_i  : in std_logic;
		-- Chip enable
		ce_i     : in std_logic;
		-- Switch
		switch_i : in std_logic;
		-- Data bus
		data_i   : in std_logic_vector(31 downto 0);
		data_o   : out std_logic_vector(31 downto 0);
		-- Bus signals
		bls_i    : in std_logic_vector(3 downto 0)
	);
	end component;

	-- IRC interconnect
	component bus_irc
	port
	(
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Data bus
		address_i    : in std_logic_vector(4 downto 0);
		ce_i         : in std_logic;
		data_i       : in std_logic_vector(31 downto 0);
		data_o       : out std_logic_vector(31 downto 0);
		--
		bls_i        : in std_logic_vector(3 downto 0);
		-- Signals for IRC
		irc_i        : in IRC_INPUT_Array_Type(7 downto 0)
	);
	end component;

	-- Measurement interconnect
	component bus_measurement
	port
	(
		-- Clock
		clk_i     : in std_logic;
		-- Reset
		reset_i   : in std_logic;
		-- Chip enable
		ce_i      : in std_logic;
		-- Address
		address_i : in std_logic_vector(1 downto 0);
		-- Data bus
		data_i    : in std_logic_vector(31 downto 0);
		data_o    : out std_logic_vector(31 downto 0);
		-- Bus signals
		bls_i     : in std_logic_vector(3 downto 0)
	);
	end component;

	-- Tumbl interconnect
	component bus_tumbl
	port
	(
		-- Clock
		clk_i        : in std_logic;
		-- Chip enable
		ce_i         : in std_logic;
		-- Global Reset
		reset_i      : in std_logic;
		-- Master CPU bus for the memory
    bls_i        : in std_logic_vector(3 downto 0);
    address_i    : in std_logic_vector(11 downto 0);
    data_i       : in std_logic_vector(31 downto 0);
    data_o       : out std_logic_vector(31 downto 0);
		-- Tumbl extrenal memory bus
		xmemb_sel_o  : out std_logic;
		xmemb_i      : in DMEMB2CORE_Type;
		xmemb_o      : out CORE2DMEMB_Type
  );
	end component;

	-- Register on the bus
	component bus_register is
	generic
	(
		-- Reset value
		reset_value_g : std_logic_vector(31 downto 0) := (others => '0');
		-- Width
		b0_g          : natural := 8;
		b1_g          : natural := 8;
		b2_g          : natural := 8;
		b3_g          : natural := 8
	);
	port
	(
		-- Clock
		clk_i         : in std_logic;
		-- Reset
		reset_i       : in std_logic;
		-- Chip enable
		ce_i          : in std_logic;
		-- Data bus
		data_i        : in std_logic_vector((b0_g+b1_g+b2_g+b3_g-1) downto 0);
		data_o        : out std_logic_vector((b0_g+b1_g+b2_g+b3_g-1) downto 0);
		-- Bus signals
		bls_i         : in std_logic_vector(3 downto 0)
	);
	end component;

	-- LX Master bus interconnect
	component bus_lxmaster
	port
	(
		clk_i        : in std_logic;
		reset_i      : in std_logic;
		-- Data bus
		address_i    : in std_logic_vector(10 downto 0);
		ce_i         : in std_logic;
		data_i       : in std_logic_vector(15 downto 0);
		data_o       : out std_logic_vector(15 downto 0);
		--
		bls_i        : in std_logic_vector(1 downto 0);
		--
		rx_done_o    : out std_logic;
		-- Signals for LX Master
		clock_i      : in std_logic;
		miso_i       : in std_logic;
		sync_i       : in std_logic;
		--
		clock_o      : out std_logic;
		mosi_o       : out std_logic;
		sync_o       : out std_logic
	);
	end component;

	--------------------------------------------------------------------------------
	-- BRAM
	--------------------------------------------------------------------------------
	type BRAM_type is (READ_FIRST, WRITE_FIRST, NO_CHANGE);

	component xilinx_dualport_bram
	generic
	(
		byte_width    : positive := 8;
		address_width : positive := 8;
		we_width      : positive := 4;
		port_a_type   : BRAM_type := READ_FIRST;
		port_b_type   : BRAM_type := READ_FIRST
	);
	port
	(
		clka  : in std_logic;
		rsta  : in std_logic;
		ena   : in std_logic;
		wea   : in std_logic_vector((we_width-1) downto 0);
		addra : in std_logic_vector((address_width-1) downto 0);
		dina  : in std_logic_vector(((byte_width*we_width)-1) downto 0);
		douta : out std_logic_vector(((byte_width*we_width)-1) downto 0);
		clkb  : in std_logic;
		rstb  : in std_logic;
		enb   : in std_logic;
		web   : in std_logic_vector((we_width-1) downto 0);
		addrb : in std_logic_vector((address_width-1) downto 0);
		dinb  : in std_logic_vector(((byte_width*we_width)-1) downto 0);
		doutb : out std_logic_vector(((byte_width*we_width)-1) downto 0)
	);
	end component;

end lx_rocon_pkg;

package body lx_rocon_pkg is

end lx_rocon_pkg;
