--
-- * Counter - divider *
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <ppisa@pikron.com>
--
-- license: BSD
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cnt_div is
	generic (
		cnt_width_g : natural := 8
	);
	port
	(
		clk_i     : in std_logic;
		en_i      : in std_logic;
		reset_i   : in std_logic;
		ratio_i   : in std_logic_vector(cnt_width_g-1 downto 0);
		q_out_o   : out std_logic
	);
end cnt_div;

architecture behavioral of cnt_div is
	signal cnt_val_s : natural range 0 to (2**cnt_width_g - 1);
	signal cnt_val_r : natural range 0 to (2**cnt_width_g - 1);
begin

comb: process (reset_i, en_i, ratio_i, cnt_val_r)
	begin
		if reset_i = '1' then
			cnt_val_s <= to_integer(unsigned(ratio_i));
			q_out_o   <= '0';
		else
			if en_i = '0' then
				cnt_val_s <= cnt_val_r;
				q_out_o   <= '0';
			else
				if cnt_val_r <= 1 then
					cnt_val_s <= to_integer(unsigned(ratio_i));
					q_out_o   <= '1';
				else
					cnt_val_s <= cnt_val_r - 1;
					q_out_o   <= '0';
				end if;
			end if;
		end if;
	end process;

seq: process
	begin
		wait until clk_i'event and clk_i = '1';
		cnt_val_r <= cnt_val_s;
	end process;

end behavioral;
